﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December2Problem2
    {
        //test input:
        //static string[] input = new string[]{"abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"};
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem2-input.txt");

        public static void Main1(string[] args)
        {
            Console.WriteLine(Run());
        }

        private static string Run()
        {
            for (int i = 0; i < input.Length; i++)
            {
                for (int j = i + 1; j < input.Length; j++)
                {
                    if (DifferBy1(input[i], input[j]))
                        return input[i] + " - " + input[j];
                }
            }

            return "none";
        }

        private static bool DifferBy1(string id1, string id2)
        {
            int difference = 0;
            for (int i = 0; i < id1.Length; i++)
            {
                if (id1[i] != id2[i])
                    difference++;

                if (difference == 2)
                    return false;
            }

            if (difference == 0)
                return false;

            return true;
        }
    }
}
