﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December23Problem1
    {
        //test input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem23-input.txt");

        private class Drone
        {
            public int X;
            public int Y;
            public int Z;
            public int R;

            public bool InRangeOf(Drone drone)
            {
                int dx = Math.Abs(X - drone.X);
                int dy = Math.Abs(Y - drone.Y);
                int dz = Math.Abs(Z - drone.Z);
                return (dx + dy + dz) <= R;
            }
        }

        static List<Drone> drones = new List<Drone>();

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            // pos=<15758255,29323050,44252572>, r=75313061
            Regex inputRegex = new Regex(@"pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            for (int i = 0; i < input.Length; i++)
            {
                Match instruction = inputRegex.Match(input[i]);
                int x = int.Parse(instruction.Groups[1].Captures[0].Value);
                int y = int.Parse(instruction.Groups[2].Captures[0].Value);
                int z = int.Parse(instruction.Groups[3].Captures[0].Value);
                int r = int.Parse(instruction.Groups[4].Captures[0].Value);

                drones.Add(new Drone{X = x, Y = y, Z = z, R = r});
            }
        }

        private static string SolveProblem()
        {
            int maxRadius = drones.Max(drone => drone.R);
            Drone maxRadiusDrone = drones.Find(drone => drone.R == maxRadius);

            int answer = drones.Aggregate(0, (accumulate, drone) => accumulate + (maxRadiusDrone.InRangeOf(drone) ? 1 : 0));

            return answer.ToString();
        }
    }
}
