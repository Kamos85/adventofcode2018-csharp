﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December24Problem1
    {
        //test input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem24-test-input.txt");
        //problem input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem24-input.txt");

        private class UnitGroup
        {
            public enum ModifierType
            {
                Slashing,
                Radiation,
                Cold,
                Fire,
                Bludgeoning
            }

            public enum GroupType
            {
                ImmuneSystem,
                Infection
            }

            public GroupType Group;
            public int GroupID;
            public int UnitCount;
            public int HitPointsPerUnit;
            public ModifierType DamageModifier;
            public int Damage;
            public int Initiative;
            public HashSet<ModifierType> ImmuneModifiers = new HashSet<ModifierType>();
            public HashSet<ModifierType> WeakToModifiers = new HashSet<ModifierType>();

            private UnitGroup attackTarget;
            private UnitGroup defendAgainst;

            public int EffectivePower => UnitCount * Damage;
            public bool IsAlive => UnitCount > 0;
            public bool IsBeingAttacked => defendAgainst != null;

            public void ParseModifiers(string damageModifier, string optionalWeakImmuneModifiers)
            {
                DamageModifier = StringToModifier(damageModifier);
                optionalWeakImmuneModifiers = optionalWeakImmuneModifiers.Trim();

                if (optionalWeakImmuneModifiers.Length == 0)
                    return;

                if (optionalWeakImmuneModifiers.Contains("weak to"))
                {
                    int index = optionalWeakImmuneModifiers.IndexOf("weak to") + "weak to".Length + 1;
                    string modifierparse = "";
                    while (optionalWeakImmuneModifiers[index] != ';' && optionalWeakImmuneModifiers[index] != ')')
                    {
                        modifierparse = "";
                        while (optionalWeakImmuneModifiers[index] != ';'
                               && optionalWeakImmuneModifiers[index] != ')'
                               && optionalWeakImmuneModifiers[index] != ',')
                        {
                            modifierparse += optionalWeakImmuneModifiers[index];
                            index++;
                        }

                        if (optionalWeakImmuneModifiers[index] == ',')
                            index += 2;

                        WeakToModifiers.Add(StringToModifier(modifierparse));
                    }
                }

                if (optionalWeakImmuneModifiers.Contains("immune to"))
                {
                    int index = optionalWeakImmuneModifiers.IndexOf("immune to") + "immune to".Length + 1;
                    string modifierparse = "";
                    while (optionalWeakImmuneModifiers[index] != ';' && optionalWeakImmuneModifiers[index] != ')')
                    {
                        modifierparse = "";
                        while (optionalWeakImmuneModifiers[index] != ';'
                               && optionalWeakImmuneModifiers[index] != ')'
                               && optionalWeakImmuneModifiers[index] != ',')
                        {
                            modifierparse += optionalWeakImmuneModifiers[index];
                            index++;
                        }

                        if (optionalWeakImmuneModifiers[index] == ',')
                            index += 2;

                        ImmuneModifiers.Add(StringToModifier(modifierparse));
                    }
                }
            }

            private ModifierType StringToModifier(string modifier)
            {
                if (modifier == "slashing") return ModifierType.Slashing;
                if (modifier == "radiation") return ModifierType.Radiation;
                if (modifier == "cold") return ModifierType.Cold;
                if (modifier == "fire") return ModifierType.Fire;
                if (modifier == "bludgeoning") return ModifierType.Bludgeoning;
                throw new Exception("Unkown modifier: " + modifier);
            }

            public void ClearTargets()
            {
                attackTarget = null;
                defendAgainst = null;
            }

            public int CalculateDamageDoneByAttacker(UnitGroup attacker)
            {
                if (ImmuneModifiers.Contains(attacker.DamageModifier))
                    return 0;

                if (WeakToModifiers.Contains(attacker.DamageModifier))
                    return attacker.EffectivePower * 2;

                return attacker.EffectivePower;
            }

            public void SelectedAsTargetByAttacker(UnitGroup attacker)
            {
                defendAgainst = attacker;
                attacker.attackTarget = this;
            }

            public void DoDamage()
            {
                if (attackTarget == null)
                    return;

                if (!attackTarget.IsAlive)
                    return;

                int damage = attackTarget.CalculateDamageDoneByAttacker(this);

                int attackedUnitGroupHealth = attackTarget.UnitCount * attackTarget.HitPointsPerUnit;
                attackedUnitGroupHealth -= damage;

                int previousUnitCount = attackTarget.UnitCount;

                if (attackedUnitGroupHealth <= 0)
                    attackTarget.UnitCount = 0;
                else
                    attackTarget.UnitCount = (int)Math.Ceiling(attackedUnitGroupHealth / (float)attackTarget.HitPointsPerUnit);

                Console.WriteLine(Group + "-" + GroupID + " does " + damage + " damage to " + attackTarget.Group + "-" + attackTarget.GroupID + ", killed units: " + (previousUnitCount - attackTarget.UnitCount));
            }
        }

        private static List<UnitGroup> unitGroups = new List<UnitGroup>();

        public static void Main1(string[] args)
        {
            ParseInput();
            Console.WriteLine(SolveProblem());
        }

        private static void ParseInput()
        {
            //47 units each with 4241 hit points (weak to slashing, cold; immune to radiation) with an attack that does 889 cold damage at initiative 10
            Regex scanUnitGroup = new Regex(@"(\d+) units each with (\d+) hit points(.*)with an attack that does (\d+) (\w+) damage at initiative (\d+)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            UnitGroup.GroupType groupType = UnitGroup.GroupType.ImmuneSystem;

            int groupID = 1;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i].StartsWith("Immune System"))
                {
                    groupType = UnitGroup.GroupType.ImmuneSystem;
                    groupID = 1;
                    continue;
                }
                if (input[i].StartsWith("Infection"))
                {
                    groupType = UnitGroup.GroupType.Infection;
                    groupID = 1;
                    continue;
                }
                if (input[i].Length < 2)
                    continue; // skip empty line

                Match matchinstr = scanUnitGroup.Match(input[i]);
                int unitCount = int.Parse(matchinstr.Groups[1].Captures[0].Value);
                int hpPerUnit = int.Parse(matchinstr.Groups[2].Captures[0].Value);
                string optional = matchinstr.Groups[3].Captures[0].Value;
                int damage = int.Parse(matchinstr.Groups[4].Captures[0].Value);
                string damageType = matchinstr.Groups[5].Captures[0].Value;
                int initiative = int.Parse(matchinstr.Groups[6].Captures[0].Value);
                Console.WriteLine(unitCount + ", " + hpPerUnit + ", " + optional + ", " + damage + ", " + damageType + ", " + initiative);
                UnitGroup unitGroup = new UnitGroup
                {
                    Group = groupType,
                    UnitCount = unitCount,
                    HitPointsPerUnit = hpPerUnit,
                    Damage = damage,
                    Initiative = initiative,
                    GroupID = groupID
                };
                unitGroup.ParseModifiers(damageType, optional);
                unitGroups.Add(unitGroup);
                groupID++;
            }

        }

        private static string SolveProblem()
        {
            int round = 0;
            while (CanBattleContinue())
            {
                DoBattleRound();
                round++;

                Console.WriteLine("---------- after round " + round + "-----------");
                PrintBattleStatus();
                Console.WriteLine("----------------------------------------");
                Console.WriteLine("");
            }

            return GetUnitsAlive().ToString();
        }

        private static bool CanBattleContinue()
        {
            bool immuneAlive = false;
            bool infectionAlive = false;
            for (int i = 0; i < unitGroups.Count; i++)
            {
                if (unitGroups[i].IsAlive)
                {
                    if (unitGroups[i].Group == UnitGroup.GroupType.ImmuneSystem)
                        immuneAlive = true;
                    if (unitGroups[i].Group == UnitGroup.GroupType.Infection)
                        infectionAlive = true;
                }
            }

            return immuneAlive && infectionAlive;
        }

        private static int GetUnitsAlive()
        {
            int unitSum = 0;
            for (int i = 0; i < unitGroups.Count; i++)
                if (unitGroups[i].IsAlive)
                    unitSum += unitGroups[i].UnitCount;

            return unitSum;
        }

        private static void DoBattleRound()
        {
            for (int i = 0; i < unitGroups.Count; i++)
                unitGroups[i].ClearTargets();

            TargetSelectionPhase();
            AttackingPhase();
        }

        private static void TargetSelectionPhase()
        {
            unitGroups.Sort(
                (group1, group2) =>
                {
                    if (group1.EffectivePower > group2.EffectivePower)
                        return -1;
                    if (group1.EffectivePower < group2.EffectivePower)
                        return 1;

                    return -group1.Initiative.CompareTo(group2.Initiative);
                });

            for (int i = 0; i < unitGroups.Count; i++)
            {
                if (!unitGroups[i].IsAlive)
                    continue;

                SelectTargetForUnit(unitGroups[i]);
            }
        }

        private static void SelectTargetForUnit(UnitGroup group)
        {
            int maxDamage = -1;
            List<UnitGroup> maxDamageUnits = new List<UnitGroup>();

            for (int i = 0; i < unitGroups.Count; i++)
            {
                if (!unitGroups[i].IsAlive)
                    continue;

                if (unitGroups[i].Group == group.Group)
                    continue;

                if (unitGroups[i].IsBeingAttacked)
                    continue;

                int damage = unitGroups[i].CalculateDamageDoneByAttacker(group);
                if (damage > maxDamage)
                {
                    maxDamage = damage;
                    maxDamageUnits.Clear();
                    maxDamageUnits.Add(unitGroups[i]);
                }
                else
                {
                    if (damage == maxDamage)
                        maxDamageUnits.Add(unitGroups[i]);
                }
            }

            if (maxDamageUnits.Count == 0)
                return;

            maxDamageUnits.Sort(
                (group1, group2) =>
                {
                    if (group1.EffectivePower > group2.EffectivePower)
                        return -1;
                    if (group1.EffectivePower < group2.EffectivePower)
                        return 1;
                    return -group1.Initiative.CompareTo(group2.Initiative);
                });

            maxDamageUnits[0].SelectedAsTargetByAttacker(group);
        }

        private static void AttackingPhase()
        {
            unitGroups.Sort((group1, group2) => -group1.Initiative.CompareTo(group2.Initiative));

            for (int i = 0; i < unitGroups.Count; i++)
            {
                if (!unitGroups[i].IsAlive)
                    continue;

                unitGroups[i].DoDamage();
            }
        }

        private static void PrintBattleStatus()
        {
            UnitGroup.GroupType loggingForGroupType;
            unitGroups.Sort((group1, group2) => group1.Group.CompareTo(group2.Group));
            loggingForGroupType = unitGroups[0].Group;
            Console.WriteLine("GROUP: " + loggingForGroupType);
            for (int i = 0; i < unitGroups.Count; i++)
            {
                if (unitGroups[i].Group != loggingForGroupType)
                {
                    loggingForGroupType = unitGroups[i].Group;
                    Console.WriteLine("GROUP: " + loggingForGroupType);
                }

                Console.WriteLine("Group has units: " + unitGroups[i].UnitCount);
            }
        }
    }
}
