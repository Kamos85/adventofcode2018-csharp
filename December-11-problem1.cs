﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December11Problem1
    {
        //test input:
        //private static int gridSerialNumber = 42;

        //real input
        private static int gridSerialNumber = 9435;


        private static int[][] powerGrid = new int[300][];

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static void Setup()
        {
            for (int i = 0; i < 300; ++i)
                powerGrid[i] = new int[300];


            for (int x = 0; x < 300; x++)
            {
                int xCoord = x + 1;
                for (int y = 0; y < 300; y++)
                {
                    int yCoord = y + 1;
                    int rackID = xCoord + 10;
                    int powerLevel = rackID * yCoord;
                    powerLevel += gridSerialNumber;
                    powerLevel *= rackID;
                    powerLevel = (powerLevel / 100) % 10;
                    powerLevel -= 5;
                    powerGrid[x][y] = powerLevel;
                }
            }
        }

        private static string SolveProblem()
        {
            int maxSum = -999;
            int cx = -1, cy = -1;
            for (int x = 0; x < 300 - 2; x++)
            {
                for (int y = 0; y < 300 - 2; y++)
                {
                    int sum = 0;
                    for (int i = 0; i < 3; i++)
                        for (int j = 0; j < 3; j++)
                            sum += powerGrid[x+i][y+j];

                    if (sum > maxSum)
                    {
                        maxSum = sum;
                        cx = x;
                        cy = y;
                    }
                }
            }

            return (cx+1) + "," + (cy+1) + " -> " + maxSum;
        }
    }
}
