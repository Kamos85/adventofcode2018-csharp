﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December10Problem1
    {
        //test input:
//        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem10-test-input.txt");

        // real input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem10-input.txt");

        private class Point
        {
            public int X;
            public int Y;
            public int VX;
            public int VY;

            public void Simulate()
            {
                X += VX;
                Y += VY;
            }
        }

        private static List<Point> points = new List<Point>();

        private static int steps = 0;

        public static void Main1(string[] args)
        {
            Parse();
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static void Parse()
        {
            //position=<10, -3> velocity=<-1,  1>
            Regex rx = new Regex(@"position=\<(\s*\D?\d+)\, (\s*\D?\d+)\> velocity=\<(\s*\D?\d+)\, (\s*\D?\d+)\>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Point point;
            for (int i = 0; i < input.Length; i++)
            {
                point = new Point();
                Match match = rx.Match(input[i]);
                if (!match.Success)
                    continue;

                point.X = int.Parse(match.Groups[1].Captures[0].Value);
                point.Y = int.Parse(match.Groups[2].Captures[0].Value);
                point.VX = int.Parse(match.Groups[3].Captures[0].Value);
                point.VY = int.Parse(match.Groups[4].Captures[0].Value);
                points.Add(point);
            }
        }

        private static string SolveProblem()
        {
            string console = "";
            do
            {
                for (int i = 0; i < points.Count; i++)
                    points[i].Simulate();

                steps++;
                Console.WriteLine("steps: " + steps);

                if (PrintIfCondition())
                    console = Console.ReadLine();
                else
                    console = "";

            } while (console == "");
            return "";
        }

        private static bool PrintIfCondition()
        {
            int minX = 99999;
            int maxX = -99999;
            int minY = 99999;
            int maxY = -99999;

            for (int i = 0; i < points.Count; i++)
            {
                minX = Math.Min(minX, points[i].X);
                maxX = Math.Max(maxX, points[i].X);
                minY = Math.Min(minY, points[i].Y);
                maxY = Math.Max(maxY, points[i].Y);
            }

            bool condition = Math.Abs(minY - maxY) < 30;

            if (!condition)
                return false;

            for (int y = minY; y <= maxY; y++)
            {
                for (int x = minX; x <= maxX; x++)
                {
                    bool pointPresent = false;
                    for (int i = 0; i < points.Count; i++)
                    {
                        if (points[i].X == x && points[i].Y == y)
                        {
                            pointPresent = true;
                            break;
                        }
                    }

                    if (pointPresent)
                        Console.Write("#");
                    else
                        Console.Write(".");

                }
                Console.Write("\n");
            }

            return true;
        }
    }
}
