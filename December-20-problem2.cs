﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December20Problem2
    {
        //test input
        //private static string input = "^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$";

        //real input
        static string input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem20-input.txt")[0];

        private static List<List<char>> field = new List<List<char>>();

        private const int FIELD_SIZE = 250;

        private static int currentX = FIELD_SIZE / 2;
        private static int currentY = FIELD_SIZE / 2;

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            Stack<int> branchPointX = new Stack<int>();
            Stack<int> branchPointY = new Stack<int>();

            for (int y = 0; y < FIELD_SIZE; y++)
            {
                List<char> row = new List<char>();
                for (int x = 0; x < FIELD_SIZE; x++)
                    row.Add('#');
                field.Add(row);
            }

            field[currentY][currentX] = '.';

            for (int i = 0; i < input.Length; i++)
            {
                switch (input[i])
                {
                    case '^':
                    case '$':
                        break;

                    case '(':
                        branchPointX.Push(currentX);
                        branchPointY.Push(currentY);
                        break;

                    case ')':
                        branchPointX.Pop();
                        branchPointY.Pop();
                        break;

                    case '|':
                        currentX = branchPointX.Peek();
                        currentY = branchPointY.Peek();
                        break;

                    case 'N':
                        field[currentY-1][currentX] = '-';
                        currentY -= 2;
                        field[currentY][currentX] = '.';
                        break;
                    case 'E':
                        field[currentY][currentX+1] = '|';
                        currentX += 2;
                        field[currentY][currentX] = '.';
                        break;
                    case 'S':
                        field[currentY+1][currentX] = '-';
                        currentY += 2;
                        field[currentY][currentX] = '.';
                        break;
                    case 'W':
                        field[currentY][currentX-1] = '|';
                        currentX -= 2;
                        field[currentY][currentX] = '.';
                        break;
                }
            }

            if (branchPointX.Count != 0)
                Console.WriteLine("branchPointX should be 0, but is: " + branchPointX.Count);
        }

        private static string SolveProblem()
        {
            //PrintField();
            List<List<int>> distanceField = new List<List<int>>();

            for (int y = 0; y < FIELD_SIZE; y++)
            {
                List<int> row = new List<int>();
                for (int x = 0; x < FIELD_SIZE; x++)
                    row.Add(-1);
                distanceField.Add(row);
            }

            Queue<int> openX = new Queue<int>();
            Queue<int> openY = new Queue<int>();
            Queue<int> distance = new Queue<int>();

            openX.Enqueue(FIELD_SIZE / 2);
            openY.Enqueue(FIELD_SIZE / 2);
            distance.Enqueue(0);

            while (openX.Count != 0)
            {
                int x = openX.Dequeue();
                int y = openY.Dequeue();
                int d = distance.Dequeue();
                distanceField[y][x] = d;

                if (field[y][x+1] != '#' && distanceField[y][x+2] == -1)
                {
                    openX.Enqueue(x + 2);
                    openY.Enqueue(y);
                    distance.Enqueue(d + 1);
                }

                if (field[y][x-1] != '#' && distanceField[y][x-2] == -1)
                {
                    openX.Enqueue(x - 2);
                    openY.Enqueue(y);
                    distance.Enqueue(d + 1);
                }

                if (field[y-1][x] != '#' && distanceField[y-2][x] == -1)
                {
                    openX.Enqueue(x);
                    openY.Enqueue(y - 2);
                    distance.Enqueue(d + 1);
                }

                if (field[y+1][x] != '#' && distanceField[y+2][x] == -1)
                {
                    openX.Enqueue(x);
                    openY.Enqueue(y + 2);
                    distance.Enqueue(d + 1);
                }
            }

            int roomsWith1000OrMore = 0;
            for (int y = 0; y < FIELD_SIZE; y++)
            {
                for (int x = 0; x < FIELD_SIZE; x++)
                {
                    if (distanceField[y][x] >= 1000)
                        roomsWith1000OrMore++;
                }
            }

            //PrintField();
            return "Answer: " + roomsWith1000OrMore;
        }

        private static void PrintField()
        {
            for (int y = 0; y < FIELD_SIZE; y++)
                Console.WriteLine(string.Join("", field[y]));
        }
    }
}
