﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December4Problem2
    {
        private class EventData : IComparable
        {
            private int day;
            private int hours;
            public int minutes;
            private string eventName;

            public int guardID = -1;
            public bool fallsAsleep;

            public EventData(int day, int hours, int minutes, string eventName)
            {
                this.day = day;
                this.hours = hours;
                this.minutes = minutes;
                this.eventName = eventName;

                if (eventName.Contains("#"))
                {
                    Regex rx = new Regex(@".* \#(\d+) .*");
                    Match match = rx.Match(eventName);
                    guardID = int.Parse(match.Groups[1].Captures[0].Value);
                }
                else
                {
                    fallsAsleep = eventName.Contains("asleep");
                }
            }

            public void Print()
            {
                Console.WriteLine(day + " - " + hours + " - " + minutes + " - " + eventName + " - " + guardID);
            }

            public int CompareTo(object o)
            {
                EventData other = (EventData) o;

                if (day < other.day)
                    return -1;

                if (day == other.day)
                {
                    if (hours < other.hours)
                        return -1;

                    if (hours == other.hours)
                        return (minutes < other.minutes) ? -1 : 1;
                }

                return 1;
            }
        }

        //test input:
        //static string[] input = new string[]{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"};
        // [1518-06-06 23:58]
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem4-input.txt");
//        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem4-test-input.txt");

        static List<EventData> events = new List<EventData>();

        public static void Main1(string[] args)
        {
            ParseInput();
            Console.WriteLine(GetAnswer());
        }

        private static void ParseInput()
        {
            // [1518-11-05 00:03] Guard #99 begins shift
            Regex rx = new Regex(@"\[(\d\d\d\d)\-(\d\d)\-(\d\d) (\d\d)\:(\d\d)\] (.*)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            for (int i = 0; i < input.Length; i++)
            {
                Match match = rx.Match(input[i]);

                string year = (match.Groups[1].Captures[0].Value);
                string month = (match.Groups[2].Captures[0].Value);
                string day = (match.Groups[3].Captures[0].Value);
                string hours = (match.Groups[4].Captures[0].Value);
                string minutes = (match.Groups[5].Captures[0].Value);
                string text = match.Groups[6].Captures[0].Value;

                events.Add(new EventData(int.Parse(year+month+day), int.Parse(hours), int.Parse(minutes), text));
            }

            events.Sort();
//            for (int i = 0; i < events.Count; i++)
//                events[i].Print();
        }

        private class GuardInfo
        {
            public List<int> sleepingMinutes = new List<int>();  // registers each minute guard sleeps

            public int TotalMinutesAsleep => sleepingMinutes.Count;

            public int MaxMinuteAsleep
            {
                get
                {
                    int minuteAmountMax = 0;
                    for (int i = 0; i < 59; ++i)
                    {
                        int minuteAmount = sleepingMinutes.FindAll(j => j == i).Count;
                        if (minuteAmount > minuteAmountMax)
                            minuteAmountMax = minuteAmount;
                    }

                    return minuteAmountMax;
                }
            }

            public int MinuteMostAsleep
            {
                get
                {
                    int minuteAmountMax = 0;
                    int minuteMax = -1;
                    for (int i = 0; i < 59; ++i)
                    {
                        int minuteAmount = sleepingMinutes.FindAll(j => j == i).Count;
                        if (minuteAmount > minuteAmountMax)
                        {
                            minuteAmountMax = minuteAmount;
                            minuteMax = i;
                        }
                    }

                    return minuteMax;
                }
            }
        }

        private static Dictionary<int, GuardInfo> guardsInfo = new Dictionary<int, GuardInfo>();
        private static int GetAnswer()
        {
            int currentID = 0;
            int minuteAsleep = 0;
            for (int i = 0; i < events.Count; i++)
            {
                if (events[i].guardID >= 0)
                {
                    currentID = events[i].guardID;
                    if (!guardsInfo.ContainsKey(currentID))
                        guardsInfo[currentID] = new GuardInfo();
                }
                else
                {
                    if (events[i].fallsAsleep)
                        minuteAsleep = events[i].minutes;
                    else
                        for (int j = minuteAsleep; j < events[i].minutes; j++)
                            guardsInfo[currentID].sleepingMinutes.Add(j);
                }
            }

            GuardInfo guardMostAsleepOnAMinute = null;
            int guardMostAsleepOnAMinuteID = -1;

            foreach (var guardInfo in guardsInfo)
                if (guardMostAsleepOnAMinute == null
                    || guardMostAsleepOnAMinute.MaxMinuteAsleep < guardInfo.Value.MaxMinuteAsleep)
                {
                    guardMostAsleepOnAMinute = guardInfo.Value;
                    guardMostAsleepOnAMinuteID = guardInfo.Key;
                }

            Console.WriteLine("guardMostAsleepOnAMinuteID: " + guardMostAsleepOnAMinuteID
                                                    + ", MaxMinuteAsleep: " + guardMostAsleepOnAMinute.MaxMinuteAsleep
                                                    + ", MinuteMostAsleep: " + guardMostAsleepOnAMinute.MinuteMostAsleep);

            return guardMostAsleepOnAMinuteID * guardMostAsleepOnAMinute.MinuteMostAsleep;
        }
    }
}
