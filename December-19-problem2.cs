﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December19Problem2
    {
        //test input
        //static string[] rawinput = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem19-test-input.txt");
        
        //#ip 4
        //addi 4 16 4		IP += 16					GOTO SETUP
        //seti 1   1		B = 1							LABEL SIMULATE  (PART1: F=876) (PART2: F=10551276)
        //seti 1   2		C = 1							LABEL RESET
        //mulr 1 2 3		D = B * C						LABEL REDO
        //eqrr 3 5 3		D = (D == F) ? 1 : 0		if B*C==F
        //addr 3 4 4		IP = D + IP						A = B + A
        //addi 4 1 4		IP = IP + 1					
        //addr 1 0 0		A = B + A					
        //addi 2 1 2		C = C + 1					C++
        //gtrr 2 5 3		D = (C > F) ? 1 : 0			
        //addr 4 3 4		IP = IP + D					if C<=F
        //seti 2   4		IP = 2							GOTO REDO
        //addi 1 1 1		B += 1						B++
        //gtrr 1 5 3		D = (B > F)? 1 : 0			if B>F
        //addr 3 4 4		IP += D							EXIT
        //seti 1   4		IP = 1						GOTO RESET
        //mulr 4 4 4		IP = IP * IP				EXIT
        //
        //addi 5 2 5		F += 2						LABEL SETUP
        //mulr 5 5 5		F = F * F				F=4
        //mulr 4 5 5		F = IP * F				F=76
        //muli 5 11 5		F *= 11					F=836
        //addi 3 1 3		D++						D = 1
        //mulr 3 4 3		D = D * IP				D = 22
        //addi 3 18 3		D += 18					D = 40
        //addr 5 3 5		F += D					F=876
        //addr 4 0 4		IP += A         (part 1, A = 0, goto simulate)
        //seti 0   4		IP = 0						GOTO SIMULATE
        //setr 4   3		D = IP     (part 2, A = 1)	D=27
        //mulr 3 4 3		D *= IP					D=756
        //addr 4 3 3		D += IP					D=785
        //mulr 4 3 3		D *= IP					D=23550
        //muli 3 14 3		D *= 14					D=329700
        //mulr 3 4 3		D *= IP					D=10550400
        //addr 5 3 5		F += D					F=10551276
        //seti 0   0		A = 0
        //seti 0   4		IP = 0						GOTO SIMULATE

        //real input
        static string[] rawinput = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem19-input.txt");

        private static string[] input;

        static Regex scanInstruction = new Regex(@"(\D+) (\d+) (\d+) (\d+)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        static List<int> registers = new List<int>{1, 0, 0, 0, 0, 0};

        static int instructionCounter = 0;
        static int registerBoundToIP = -1;

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblemByCSharpCode());
        }

        private static void Setup()
        {
            if (rawinput[0].StartsWith("#ip "))
            {
                int registerCounter = int.Parse(rawinput[0].Substring("#ip ".Length));
                registerBoundToIP = registerCounter;
                Console.WriteLine("Bound IP to register " + registerBoundToIP);
            }

            input = new string[rawinput.Length - 1];

            Array.Copy(rawinput, 1, input, 0, rawinput.Length - 1);
        }

        private static string SolveProblemByCSharpCode()
        {
            // suspected that the answer is the sum of all divisors of F (in part one 876 and in part two 10551276)
            int F = 10551276;

            int disivorSum = 0;
            for (int i = 1; i <= F; i++)
                //for (int C = 1; C < F; C++)
                if (F % i == 0)
                    disivorSum += i;

            return "Answer: " + disivorSum;
        }

        private static string SolveProblem()
        {
            int executed = 0;

            while (0 <= instructionCounter && instructionCounter < input.Length)
            {
                string instr = input[instructionCounter];

                //if (executed > 1000000)
                {
//                    Console.WriteLine(instructionCounter + ": " + instr);
//                    PrintRegisters();
//                    Console.ReadLine();
                }

                if (registerBoundToIP != -1)
                    registers[registerBoundToIP] = instructionCounter;

                Match matchinstr = scanInstruction.Match(instr);
                string opcode = matchinstr.Groups[1].Captures[0].Value;
                int A = int.Parse(matchinstr.Groups[2].Captures[0].Value);
                int B = int.Parse(matchinstr.Groups[3].Captures[0].Value);
                int C = int.Parse(matchinstr.Groups[4].Captures[0].Value);

                DoInstruction(registers, opcode, A, B, C);

                if (registerBoundToIP != -1)
                    instructionCounter = registers[registerBoundToIP];

                instructionCounter++;
                executed++;
            }

            PrintRegisters();
            Console.WriteLine("Execution halted on IP: " + instructionCounter);

            return "Answer: " + registers[0];
        }

        private static void PrintRegisters()
        {
            Console.Write("\t");
            for (int i = 0; i < registers.Count; i++)
                Console.Write(registers[i] + " ");
            Console.WriteLine();
        }

        private static void DoInstruction(List<int> registersState, string opcode, int A, int B, int C)
        {
            switch (opcode)
            {
                case "addr":
                    registersState[C] = registersState[A] + registersState[B];
                    break;
                case "addi":
                    registersState[C] = registersState[A] + B;
                    break;
                case "mulr":
                    registersState[C] = registersState[A] * registersState[B];
                    break;
                case "muli":
                    registersState[C] = registersState[A] * B;
                    break;
                case "banr":
                    registersState[C] = registersState[A] & registersState[B];
                    break;
                case "bani":
                    registersState[C] = registersState[A] & B;
                    break;
                case "borr":
                    registersState[C] = registersState[A] | registersState[B];
                    break;
                case "bori":
                    registersState[C] = registersState[A] | B;
                    break;
                case "setr":
                    registersState[C] = registersState[A];
                    break;
                case "seti":
                    registersState[C] = A;
                    break;
                case "gtir":
                    registersState[C] = (A > registersState[B]) ? 1 : 0;
                    break;
                case "gtri":
                    registersState[C] = (registersState[A] > B) ? 1 : 0;
                    break;
                case "gtrr":
                    registersState[C] = (registersState[A] > registersState[B]) ? 1 : 0;
                    break;
                case "eqir":
                    registersState[C] = (A == registersState[B]) ? 1 : 0;
                    break;
                case "eqri":
                    registersState[C] = (registersState[A] == B) ? 1 : 0;
                    break;
                case "eqrr":
                    registersState[C] = (registersState[A] == registersState[B]) ? 1 : 0;
                    break;
                default:
                    Console.WriteLine("Unknown opcode: " + opcode);
                    break;
            }
        }
    }
}
