﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December3Problem2
    {
        //test input:
        //static string[] input = new string[]{"#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"};
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem3-input.txt");

        private static int[][] field;

        public static void Main1(string[] args)
        {
            field = new int[1000][];
            for (int i = 0; i < 1000; ++i)
                field[i] = new int[1000];

            Console.WriteLine(Run());
        }

        private static int Run()
        {
            Regex rx = new Regex(@"#(\d+) @ (\d+),(\d+): (\d+)x(\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            HashSet<int> processedIDs = new HashSet<int>();

            for (int i = 0; i < input.Length; i++)
            {
                // Find matches.
                Match match = rx.Match(input[i]);

                int id = int.Parse(match.Groups[1].Captures[0].Value);
                int left = int.Parse(match.Groups[2].Captures[0].Value);
                int top = int.Parse(match.Groups[3].Captures[0].Value);
                int width = int.Parse(match.Groups[4].Captures[0].Value);
                int height = int.Parse(match.Groups[5].Captures[0].Value);

                processedIDs.Add(id);

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        if (field[y + top][x + left] > 0)
                        {
                            processedIDs.Remove(field[y + top][x + left]);
                            processedIDs.Remove(id);
                        }

                        field[y + top][x + left] = id;
                    }
                }
            }

            return processedIDs.ElementAt(0);
        }
    }
}
