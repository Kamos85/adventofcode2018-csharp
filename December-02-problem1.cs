﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December2Problem1
    {
        //test input:
        //static string[] input = new string[]{"abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"};
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem2-input.txt");

        public static void Main1(string[] args)
        {
            Console.WriteLine(Run());
        }

        private static int Run()
        {
            int bagIDOf2s = 0;
            int bagIDOf3s = 0;

            for (int i = 0; i < input.Length; i++)
            {
                string id = input[i];
                if (CountBoxLetters(id, 2))
                    ++bagIDOf2s;
                if (CountBoxLetters(id, 3))
                    ++bagIDOf3s;
            }
            return bagIDOf2s * bagIDOf3s;
        }

        private static bool CountBoxLetters(string id, int letterCount)
        {
            Dictionary<char, int> bag = new Dictionary<char, int>();
            foreach (char c in id)
            {
                if (!bag.ContainsKey(c))
                    bag[c] = 1;
                else
                    bag[c] += 1;
            }

            foreach (var pair in bag)
                if (pair.Value == letterCount)
                    return true;

            return false;
        }
    }
}
