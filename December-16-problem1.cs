﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December16Problem1
    {
        //real input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem16-1-input.txt");

        private static Regex scanRegisters = new Regex(@"(\d+),?\s(\d+),?\s(\d+),?\s(\d+)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private class TestCase
        {
            private List<int> registersBefore = new List<int>();
            private List<int> registerAfter = new List<int>();
            private int opcode;
            private int A;
            private int B;
            private int C;

            private int matchedOpcodeCount = 0;
            public int MatchedOpcodeCount
            {
                get { return matchedOpcodeCount; }
            }

            public TestCase(string before, string instruction, string after)
            {
                Match matchbefore = scanRegisters.Match(before);
                registersBefore.Add(int.Parse(matchbefore.Groups[1].Captures[0].Value));
                registersBefore.Add(int.Parse(matchbefore.Groups[2].Captures[0].Value));
                registersBefore.Add(int.Parse(matchbefore.Groups[3].Captures[0].Value));
                registersBefore.Add(int.Parse(matchbefore.Groups[4].Captures[0].Value));

                Match matchinstr = scanRegisters.Match(instruction);
                opcode = int.Parse(matchinstr.Groups[1].Captures[0].Value);
                A = int.Parse(matchinstr.Groups[2].Captures[0].Value);
                B = int.Parse(matchinstr.Groups[3].Captures[0].Value);
                C = int.Parse(matchinstr.Groups[4].Captures[0].Value);

                Match matchafter = scanRegisters.Match(after);
                registerAfter.Add(int.Parse(matchafter.Groups[1].Captures[0].Value));
                registerAfter.Add(int.Parse(matchafter.Groups[2].Captures[0].Value));
                registerAfter.Add(int.Parse(matchafter.Groups[3].Captures[0].Value));
                registerAfter.Add(int.Parse(matchafter.Groups[4].Captures[0].Value));

                Console.WriteLine(string.Join(",", registersBefore) + " -> " + opcode + " " + A + " " + B + " " + C + " -> " + string.Join(",", registerAfter));
            }

            public void Run()
            {
                for (int i = 0; i < 16; i++)
                {
                    List<int> test = new List<int>(registersBefore);

                    DoInstruction(test, i, A, B, C);

                    bool beforeMatchAfter = true;
                    for (int j = 0; j < 4; j++)
                        if (test[j] != registerAfter[j])
                            beforeMatchAfter = false;

                    if (beforeMatchAfter)
                        matchedOpcodeCount = MatchedOpcodeCount + 1;
                }

                Console.WriteLine("matchedOpCodeCount: " + MatchedOpcodeCount);
            }
        }

        private static List<TestCase> testCases = new List<TestCase>();

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            int i = 0;
            while (i < input.Length)
            {
                string beforeState = input[i + 0].Substring("Before: ".Length);
                string instruction = input[i + 1];
                string afterState = input[i + 2].Substring("After:  ".Length);
                testCases.Add(new TestCase(beforeState, instruction, afterState));
                i += 4;
            }
        }

        private static string SolveProblem()
        {
            int match3orMoreOpcode = 0;
            for (int i = 0; i < testCases.Count; i++)
            {
                testCases[i].Run();
                if (testCases[i].MatchedOpcodeCount >= 3)
                    match3orMoreOpcode++;
            }
            return "Answer: " + match3orMoreOpcode;
        }

        private static void DoInstruction(List<int> registersState, int opcode, int A, int B, int C)
        {
            switch (opcode)
            {
                case 0: // addr
                    registersState[C] = registersState[A] + registersState[B];
                    break;
                case 1: // addi
                    registersState[C] = registersState[A] + B;
                    break;
                case 2: // mulr
                    registersState[C] = registersState[A] * registersState[B];
                    break;
                case 3: // muli
                    registersState[C] = registersState[A] * B;
                    break;
                case 4: // banr
                    registersState[C] = registersState[A] & registersState[B];
                    break;
                case 5: // bani
                    registersState[C] = registersState[A] & B;
                    break;
                case 6: // borr
                    registersState[C] = registersState[A] | registersState[B];
                    break;
                case 7: // bori
                    registersState[C] = registersState[A] | B;
                    break;
                case 8: // setr
                    registersState[C] = registersState[A];
                    break;
                case 9: // seti
                    registersState[C] = A;
                    break;
                case 10: // gtir
                    registersState[C] = (A > registersState[B]) ? 1 : 0;
                    break;
                case 11: // gtri
                    registersState[C] = (registersState[A] > B) ? 1 : 0;
                    break;
                case 12: // gtrr
                    registersState[C] = (registersState[A] > registersState[B]) ? 1 : 0;
                    break;
                case 13: // eqir
                    registersState[C] = (A == registersState[B]) ? 1 : 0;
                    break;
                case 14: // eqri
                    registersState[C] = (registersState[A] == B) ? 1 : 0;
                    break;
                case 15: // eqrr
                    registersState[C] = (registersState[A] == registersState[B]) ? 1 : 0;
                    break;
            }
        }
    }
}
