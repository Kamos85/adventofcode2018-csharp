﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December7Problem1
    {
        //test input:
//        private static string[] input =
//        {
//            "Step C must be finished before step A can begin.",
//            "Step C must be finished before step F can begin.",
//            "Step A must be finished before step B can begin.",
//            "Step A must be finished before step D can begin.",
//            "Step B must be finished before step E can begin.",
//            "Step D must be finished before step E can begin.",
//            "Step F must be finished before step E can begin."
//        };
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem7-input.txt");

        static Dictionary<string, List<string>> dependencies = new Dictionary<string, List<string>>();

        public static void Main1(string[] args)
        {
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static string SolveProblem()
        {
            Regex rx = new Regex(@"Step (.) must be finished before step (.) can begin\.", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            for (int i = 0; i < input.Length; i++)
            {
                Match match = rx.Match(input[i]);
                string step1 = match.Groups[1].Captures[0].Value;
                string step2 = match.Groups[2].Captures[0].Value;

                if (!dependencies.ContainsKey(step2))
                    dependencies[step2] = new List<string>();

                if (!dependencies.ContainsKey(step1))
                    dependencies[step1] = new List<string>();

                dependencies[step2].Add(step1);
            }

            string steps = "";
            while (true)
            {
                var elligibleSteps = dependencies.Where(pair => pair.Value.Count == 0);
                var firstStep = "ZZZ";
                foreach (var pair in elligibleSteps)
                    if (firstStep.CompareTo(pair.Key) > 0)
                        firstStep = pair.Key;

                if (firstStep == "ZZZ")
                    break;

                foreach (var pair in dependencies)
                    pair.Value.Remove(firstStep);

                dependencies.Remove(firstStep);
                steps += firstStep;
            }

            return steps;
        }
    }
}
