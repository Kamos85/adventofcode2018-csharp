﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December12Problem2
    {
        //test input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem12-test-input.txt");

        //real input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem12-input.txt");

        private static int generations = 99;
        private static int leftPadding = 5;

        private static string currentState = "";
        static Dictionary<string, bool> rules = new Dictionary<string, bool>();

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            for (int i = 0; i < leftPadding; i++)
                currentState += ".";
            currentState += input[0].Replace("initial state: ", "");
            for (int i = 0; i < generations; i++)
                currentState += ".";

            Regex rx = new Regex(@"(.{5}) \=\> (.)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            for (int i = 2; i < input.Length; i++)
            {
                Match match = rx.Match(input[i]);

                string inputRule = match.Groups[1].Captures[0].Value;
                string outputRule = match.Groups[2].Captures[0].Value;

                //Console.WriteLine(inputRule + "=>" + outputRule);
                rules[inputRule] = outputRule == "#";
            }
        }

        private static string SolveProblem()
        {
            string current = currentState;
            string next = currentState;
            string chunk;
            for (int generation = 0; generation < generations; generation++)
            {
                Console.WriteLine(generation.ToString("D3") + " " + next);

                for (int i = 0; i < current.Length - 5; i++)
                {
                    chunk = current.Substring(i, 5);
                    next = next.Remove(i+2, 1);
                    if (rules.ContainsKey(chunk) && rules[chunk])
                        next = next.Insert(i+2, "#");
                    else
                        next = next.Insert(i+2, ".");
                }
                current = next;
            }

            Console.WriteLine(generations.ToString("D3") + " " + next);

            int index = -leftPadding;
            List<int> indicesWithPlant = new List<int>();
            for (int i = 0; i < current.Length; i++)
            {
                if (current[i] == '#')
                    indicesWithPlant.Add(index);
                index++;
            }

            long skippedGeneration = 50000000000 - generations;
            long sum = 0;
            for (int i = 0; i < indicesWithPlant.Count; i++)
                sum += indicesWithPlant[i] + skippedGeneration;

            return sum.ToString();
        }
    }
}
