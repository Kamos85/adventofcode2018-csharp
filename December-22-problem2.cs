﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2018
{
    internal class December22Problem2Faster
    {
        // test input
//        const int DEPTH = 510;
//        private const int TARGET_X = 10;
//        private const int TARGET_Y = 10;

        // real input
        const int DEPTH = 11541;
        const int TARGET_X = 14;
        const int TARGET_Y = 778;

        // test
//        private const int DEPTH = 11109;
//        private const int TARGET_X = 10;
//        private const int TARGET_Y = 11;

        public enum TerrainType
        {
            Rocky = 0,
            Wet = 1,
            Narrow = 2
        }

        public enum Equipment
        {
            None = 0,
            Torch = 1,
            ClimbingGear = 2
        }

        private static Dictionary<string, Equipment> fromToTerrainEquipmentLookUp = new Dictionary<string, Equipment>()
        {
            {TerrainType.Rocky.ToString() + TerrainType.Wet.ToString(), Equipment.ClimbingGear},
            {TerrainType.Wet.ToString() + TerrainType.Rocky.ToString(), Equipment.ClimbingGear},

            {TerrainType.Wet.ToString() + TerrainType.Narrow.ToString(), Equipment.None},
            {TerrainType.Narrow.ToString() + TerrainType.Wet.ToString(), Equipment.None},

            {TerrainType.Narrow.ToString() + TerrainType.Rocky.ToString(), Equipment.Torch},
            {TerrainType.Rocky.ToString() + TerrainType.Narrow.ToString(), Equipment.Torch}
        };

        private static Dictionary<TerrainType, List<Equipment>> terrainAllowedEquipment = new Dictionary<TerrainType, List<Equipment>>()
        {
            {TerrainType.Rocky, new List<Equipment>() {Equipment.Torch, Equipment.ClimbingGear}},
            {TerrainType.Wet, new List<Equipment>() {Equipment.None, Equipment.ClimbingGear}},
            {TerrainType.Narrow, new List<Equipment>() {Equipment.None, Equipment.Torch}}
        };

        private const int PADDING = 50;
        private const int FIELD_SIZE_X = TARGET_X + 1 + PADDING;
        private const int FIELD_SIZE_Y = TARGET_Y + 1 + PADDING;

        static int[][] erosionField = new int[FIELD_SIZE_Y][];
        static TerrainType[][] terrainTypeField = new TerrainType[FIELD_SIZE_Y][];

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

//        public static void SetupTest()
//        {
//            terrainTypeField[0] = new TerrainType[]{TerrainType.Rocky, TerrainType.Narrow, TerrainType.Narrow};
//            terrainTypeField[1] = new TerrainType[]{TerrainType.Rocky, TerrainType.Wet, TerrainType.Narrow};
//            terrainTypeField[2] = new TerrainType[]{TerrainType.Rocky, TerrainType.Wet, TerrainType.Narrow};
//            terrainTypeField[3] = new TerrainType[]{TerrainType.Rocky, TerrainType.Wet, TerrainType.Rocky};
//        }

        private static void Setup()
        {
            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                int[] row = new int[FIELD_SIZE_X];
                for (int x = 0; x < FIELD_SIZE_X; x++)
                    row[x] = 0;
                erosionField[y] = row;
            }

            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                TerrainType[] row = new TerrainType[FIELD_SIZE_X];
                for (int x = 0; x < FIELD_SIZE_X; x++)
                    row[x] = ProcessFieldValue(y, x);
                terrainTypeField[y] = row;
            }

            PrintField();
        }

        private static TerrainType ProcessFieldValue(int y, int x)
        {
            int geologicalIndex = GetGeologicalIndex(y, x);
            erosionField[y][x] = (geologicalIndex + DEPTH) % 20183;
            return (TerrainType)(erosionField[y][x] % 3);
        }

        private static int GetGeologicalIndex(int y, int x)
        {
            if (y == 0 && x == 0)
                return 0;
            if (y == TARGET_Y && x == TARGET_X)
                return 0;
            if (y == 0)
                return x * 16807;
            if (x == 0)
                return y * 48271;
            return erosionField[y][x-1] * erosionField[y-1][x];
        }

        private class CellState
        {
            public int X;
            public int Y;
            public Dictionary<Equipment, int> MinutesSpentWithEquipment = new Dictionary<Equipment, int>()
            {
                {Equipment.None, 999999},
                {Equipment.Torch, 999999},
                {Equipment.ClimbingGear, 999999}
            };

            public bool possiblePath = false;
        }

        private static int[] neighBoursX = {1, 0, -1,  0};
        private static int[] neighBoursY = {0, 1,  0, -1};

        private static string SolveProblem()
        {
            CellState[][] computeField = new CellState[FIELD_SIZE_Y][];
            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                CellState[] row = new CellState[FIELD_SIZE_X];
                for (int x = 0; x < FIELD_SIZE_X; x++)
                    row[x] = new CellState{X = x, Y = y};
                computeField[y] = row;
            }

            computeField[0][0].MinutesSpentWithEquipment[Equipment.Torch] = 0;

            Queue<CellState> toProcess = new Queue<CellState>();
            toProcess.Enqueue(computeField[0][0]);

            CellState current;
            while (toProcess.Count != 0)
            {
                current = toProcess.Dequeue();
                for (int i = 0; i < neighBoursX.Length; i++)
                {
                    int nX = current.X + neighBoursX[i];
                    int nY = current.Y + neighBoursY[i];

                    if (nX < 0 || nX >= FIELD_SIZE_X
                        || nY < 0 || nY >= FIELD_SIZE_Y)
                    {
                        continue;
                    }

                    TerrainType currentTerrain = terrainTypeField[current.Y][current.X];
                    TerrainType targetTerrain = terrainTypeField[nY][nX];

                    for (int j = 0; j < terrainAllowedEquipment[currentTerrain].Count; j++)
                    {
                        Equipment currentEquipment = terrainAllowedEquipment[currentTerrain][j];
                        int minutesForStep = current.MinutesSpentWithEquipment[currentEquipment];
                        if (minutesForStep >= 999999)
                            continue;

                        Equipment requiredEquipment;
                        if (currentTerrain != targetTerrain)
                            requiredEquipment = GetEquipmentFromTo(currentTerrain, targetTerrain);
                        else
                            requiredEquipment = currentEquipment;

                        int targetMinutesForStep;
                        if (requiredEquipment == currentEquipment)
                            targetMinutesForStep = minutesForStep + 1;
                        else
                            targetMinutesForStep = minutesForStep + 1 + 7;

                        if (computeField[nY][nX].MinutesSpentWithEquipment[requiredEquipment] > targetMinutesForStep)
                        {
                            computeField[nY][nX].MinutesSpentWithEquipment[requiredEquipment] = targetMinutesForStep;
                            toProcess.Enqueue(computeField[nY][nX]);
                        }
                    }
                }
            }
            Console.WriteLine("Torch equipment: " + computeField[TARGET_Y][TARGET_X].MinutesSpentWithEquipment[Equipment.Torch]);
            Console.WriteLine("Climbing Gear equipment: " + computeField[TARGET_Y][TARGET_X].MinutesSpentWithEquipment[Equipment.ClimbingGear]);

            int answer = Math.Min(computeField[TARGET_Y][TARGET_X].MinutesSpentWithEquipment[Equipment.Torch],
                computeField[TARGET_Y][TARGET_X].MinutesSpentWithEquipment[Equipment.ClimbingGear] + 7);

            return answer.ToString();
        }

        private static bool ListEqual(List<Equipment> a, List<Equipment> b)
        {
            if (a.Count != b.Count)
                return false;
            for (int i = 0; i < a.Count; i++)
                if (!b.Contains(a[i]))
                    return false;
            return true;
        }

        private static bool ListComplement(List<Equipment> complemented, List<Equipment> additions)
        {
            bool updated = false;
            for (int i = 0; i < additions.Count; i++)
            {
                if (!complemented.Contains(additions[i]))
                {
                    complemented.Add(additions[i]);
                    updated = true;
                }
            }
            return updated;
        }

        private static Equipment GetEquipmentFromTo(TerrainType from, TerrainType to)
        {
            return fromToTerrainEquipmentLookUp[from.ToString() + to.ToString()];
        }

        private static void PrintField()
        {
            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                for (int x = 0; x < FIELD_SIZE_X; x++)
                {
                    switch (terrainTypeField[y][x])
                    {
                        case TerrainType.Rocky:
                            Console.Write(".");
                            break;
                        case TerrainType.Wet:
                            Console.Write("=");
                            break;
                        case TerrainType.Narrow:
                            Console.Write("|");
                            break;
                    }
                }

                Console.WriteLine();
            }
        }
    }
}
