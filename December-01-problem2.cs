﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December1Problem2
    {
        static int frequency = 0;
        //test input: static string[] input = new string[]{"+3", "+3", "+4", "-2", "-4"};
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem1-input.txt");

        static HashSet<int> seenFrequencies = new HashSet<int> { 0 };

        public static void Main1(string[] args)
        {
            Console.WriteLine(Run());
        }

        private static int Run()
        {
            while (true)
            {
                for (int i = 0; i < input.Length; i++)
                {
                    int number = int.Parse(input[i]);
                    frequency += number;

                    if (seenFrequencies.Contains(frequency))
                        return frequency;

                    seenFrequencies.Add(frequency);
                }
            }
        }
    }
}
