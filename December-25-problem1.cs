﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December25Problem1
    {
        //test input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem25-test-input.txt");
        //problem input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem25-input.txt");

        public class Vector4
        {
            public int X;
            public int Y;
            public int Z;
            public int W;

            public int ManHatDist(Vector4 other)
            {
                return Math.Abs(X - other.X) + Math.Abs(Y - other.Y) + Math.Abs(Z - other.Z) + Math.Abs(W - other.W);
            }
        }

        private static List<List<Vector4>> clusters = new List<List<Vector4>>();

        public static void Main(string[] args)
        {
            ParseInput();
            Console.WriteLine(SolveProblem());
        }

        private static void ParseInput()
        {
            //-1,2,2,0
            Regex scanCoord = new Regex(@"(-?\d+),(-?\d+),(-?\d+),(-?\d+)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            for (int i = 0; i < input.Length; i++)
            {
                Match matchinstr = scanCoord.Match(input[i]);
                int x = int.Parse(matchinstr.Groups[1].Captures[0].Value);
                int y = int.Parse(matchinstr.Groups[2].Captures[0].Value);
                int z = int.Parse(matchinstr.Groups[3].Captures[0].Value);
                int w = int.Parse(matchinstr.Groups[4].Captures[0].Value);
                Vector4 newVector4 = new Vector4 {X = x, Y = y, Z = z, W = w};
                HandleNewCoord(newVector4);
            }
        }

        private static void HandleNewCoord(Vector4 newCoord)
        {
            List<int> mergeClusterIndices = new List<int>();

            // first determine in which cluster(s) the new coord belongs
            for (int i = 0; i < clusters.Count; i++)
            {
                List<Vector4> cluster = clusters[i];
                for (int j = 0; j < cluster.Count; j++)
                {
                    if (cluster[j].ManHatDist(newCoord) <= 3)
                    {
                        mergeClusterIndices.Add(i);
                        break;
                    }
                }
            }

            if (mergeClusterIndices.Count == 0)
            {
                clusters.Add(new List<Vector4>{newCoord});
                return;
            }

            List<Vector4> mainCluster = clusters[mergeClusterIndices[0]];
            mainCluster.Add(newCoord);

            if (mergeClusterIndices.Count == 1)
                return;

            // multiple clusters need to be merged into one big cluster
            for (int i = mergeClusterIndices.Count - 1; i > 0; i--)
            {
                mainCluster.AddRange(clusters[mergeClusterIndices[i]]);
                clusters.RemoveAt(mergeClusterIndices[i]);
            }
        }

        private static string SolveProblem()
        {
            return clusters.Count.ToString();

            // 405 - too high
        }
    }
}
