﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December5Problem2
    {
        //test input:
//        private static string input = "dabAcCaCBAcCcaDA";
        static string input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem5-input.txt")[0];

        public static void Main1(string[] args)
        {
            React();

            string reactedInput = input;

            int smallestReactLeftOver = 999999;

            for (int i = 'a'; i <= (int) 'z'; ++i)
            {
                input = reactedInput;
                input = input.Replace(((char)i).ToString(), "");
                input = input.Replace(((char)(i-32)).ToString(), "");

                React();

                if (input.Length < smallestReactLeftOver)
                    smallestReactLeftOver = input.Length;
            }

            Console.WriteLine(smallestReactLeftOver);
        }

        private static void React()
        {
            int length = 0;

            while (length != input.Length)
            {
                length = input.Length;

                int i = 0;
                while (i < input.Length - 1)
                {
                    if (Math.Abs((int) input[i] - (int) input[i + 1]) == 32)
                        input = input.Substring(0, i) + input.Substring(i+2);
                    else
                        ++i;
                }
            }
        }
    }
}
