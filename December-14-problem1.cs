﻿using System;
using System.Collections.Generic;

namespace AdventOfCode2018
{
    internal class December14Problem1
    {
        private const int RECIPE_ANSWER_COUNT = 10;

        //test input
        //static int input = 2018;

        //real input
        static int input = 409551;

        static List<int> recipeScores = new List<int>{3, 7};

        static List<int> elfAtScoreIndices = new List<int>{0, 1};

        public static void Main1(string[] args)
        {
            Console.WriteLine(SolveProblem());
        }

        private static string SolveProblem()
        {
            int recipesMade = 2;
            while (true)
            {
                int recipeSum = 0;
                for (int j = 0; j < elfAtScoreIndices.Count; j++)
                    recipeSum += recipeScores[elfAtScoreIndices[j]];

                // create new recipes
                int newRecipe1 = -1;
                int newRecipe2 = -1;
                if (recipeSum > 9)
                {
                    newRecipe1 = recipeSum / 10;
                    newRecipe2 = recipeSum % 10;
                    recipesMade += 2;
                }
                else
                {
                    newRecipe1 = recipeSum;
                    recipesMade++;
                }

                recipeScores.Add(newRecipe1);
                if (newRecipe2 >= 0)
                    recipeScores.Add(newRecipe2);

                // move elfes
                for (int j = 0; j < elfAtScoreIndices.Count; j++)
                {
                    int moveForward = recipeScores[elfAtScoreIndices[j]] + 1;
                    elfAtScoreIndices[j] = (elfAtScoreIndices[j] + moveForward) % recipeScores.Count;
                }

                if (recipesMade > input + RECIPE_ANSWER_COUNT)
                    break;

//                for (int j = 0; j < recipeScores.Count; j++)
//                    Console.Write(recipeScores[j]);
//                Console.WriteLine();
            }

            string answer = "";
            for (int i = input; i < input + RECIPE_ANSWER_COUNT; i++)
                answer += recipeScores[i];

            return answer;
        }
    }
}
