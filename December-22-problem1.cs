﻿using System;

namespace AdventOfCode2018
{
    internal class December22Problem1
    {
        // test input
//        const int DEPTH= 510;
//        const int TARGET_X = 10;
//        const int TARGET_Y = 10;

        // real input
        const int DEPTH = 11541;
        const int TARGET_X = 14;
        const int TARGET_Y = 778;

        private const int FIELD_SIZE_X = TARGET_X + 1 + 2;
        private const int FIELD_SIZE_Y = TARGET_Y + 1 + 2;

        static int[][] erosionField = new int[FIELD_SIZE_Y][];
        static int[][] terrainField = new int[FIELD_SIZE_Y][];

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                int[] row = new int[FIELD_SIZE_X];
                for (int x = 0; x < FIELD_SIZE_X; x++)
                    row[x] = 0;
                erosionField[y] = row;
            }

            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                int[] row = new int[FIELD_SIZE_X];
                for (int x = 0; x < FIELD_SIZE_X; x++)
                    row[x] = ProcessFieldValue(y, x);
                terrainField[y] = row;
            }
        }

        private static int ProcessFieldValue(int y, int x)
        {
            int geologicalIndex = GetGeologicalIndex(y, x);
            erosionField[y][x] = (geologicalIndex + DEPTH) % 20183;
            return erosionField[y][x] % 3;
        }

        private static int GetGeologicalIndex(int y, int x)
        {
            if (y == 0 && x == 0)
                return 0;
            if (y == TARGET_Y && x == TARGET_X)
                return 0;
            if (y == 0)
                return x * 16807;
            if (x == 0)
                return y * 48271;
            return erosionField[y][x-1] * erosionField[y-1][x];
        }

        private static string SolveProblem()
        {
            PrintField();

            int sum = 0;
            for (int y = 0; y <= TARGET_Y; y++)
                for (int x = 0; x <= TARGET_X; x++)
                    sum += terrainField[y][x];

            return "" + sum;
        }

        private static void PrintField()
        {
            for (int y = 0; y < FIELD_SIZE_Y; y++)
            {
                for (int x = 0; x < FIELD_SIZE_X; x++)
                    Console.Write(terrainField[y][x]);
                Console.WriteLine();
            }
        }
    }
}
