﻿using System;
using System.IO;
using System.Linq;

namespace AdventOfCode2018
{
    internal class December1Problem1
    {
        public static void Main1(string[] args)
        {
            int sum = 0;
            // test: string[] input = new string[]{"1", "1", "-2"};
            string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem1-input.txt");

            for (int i = 0; i < input.Length; i++)
            {
                int number = int.Parse(input[i]);
                sum += number;
            }

            Console.WriteLine(sum);

            // in hindsight, much shorter:
            Console.WriteLine(input.Sum((s) => int.Parse(s)));
        }
    }
}
