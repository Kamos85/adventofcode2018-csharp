﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December9Problem1
    {
        //test input:
//        private static int players = 9;
//        private static int lastMarbleScore = 25;
        //test input:
        private static int players = 10;
        private static int lastMarbleScore = 1618;


        // real input
//        private static int players = 423;
//        private static int lastMarbleScore = 71944;

        static List<int> playerScores = new List<int>();
        static int currentPlayer = 0;

        static List<int> marbleCircle = new List<int>();
        static int currentMarbleIndex = 0;

        public static void Main1(string[] args)
        {
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static string SolveProblem()
        {
            for (int i = 0; i < players; i++)
                playerScores.Add(0);

            marbleCircle.Add(0);

            for (int i = 1; i <= lastMarbleScore; i++)
            {
                if (i % 23 == 0)
                {
                    playerScores[currentPlayer] += i;
                    currentMarbleIndex -= 7;
                    if (currentMarbleIndex < 0)
                        currentMarbleIndex += marbleCircle.Count;
                    playerScores[currentPlayer] += marbleCircle[currentMarbleIndex];
                    marbleCircle.RemoveAt(currentMarbleIndex);
                    if (currentMarbleIndex == marbleCircle.Count)
                    {
                        currentMarbleIndex = 0;
                        Console.WriteLine("OCCURS!");
                    }
                }
                else
                {
                    currentMarbleIndex = (currentMarbleIndex + 1) % marbleCircle.Count + 1;
                    if (currentMarbleIndex == 0)
                    {
                        marbleCircle.Add(i);
                        currentMarbleIndex = marbleCircle.Count - 1;
                    }
                    else
                        marbleCircle.Insert(currentMarbleIndex, i);
                }

                currentPlayer = (currentPlayer + 1) % players;

                //Console.WriteLine("[" + currentPlayer + "] " + String.Join(" ", marbleCircle) + " --- " + currentMarbleIndex);
            }
            return playerScores.Max().ToString();
        }
    }
}
