﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December19Problem1
    {
        //test input
        //static string[] rawinput = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem19-test-input.txt");

        //real input
        static string[] rawinput = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem19-input.txt");

        private static string[] input;

        static Regex scanInstruction = new Regex(@"(\D+) (\d+) (\d+) (\d+)",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        static List<int> registers = new List<int>{0, 0, 0, 0, 0, 0};

        static int instructionCounter = 0;
        static int registerBoundToIP = -1;

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            if (rawinput[0].StartsWith("#ip "))
            {
                int registerCounter = int.Parse(rawinput[0].Substring("#ip ".Length));
                registerBoundToIP = registerCounter;
                Console.WriteLine("Bound IP to register " + registerBoundToIP);
            }

            input = new string[rawinput.Length - 1];

            Array.Copy(rawinput, 1, input, 0, rawinput.Length - 1);
        }

        private static string SolveProblem()
        {
            int executed = 0;

            while (0 <= instructionCounter && instructionCounter < input.Length)
            {
                string instr = input[instructionCounter];

                //if (executed > 1000000)
                {
//                    Console.WriteLine(instructionCounter + ": " + instr);
//                    PrintRegisters();
//                    Console.ReadLine();
                }

                if (registerBoundToIP != -1)
                    registers[registerBoundToIP] = instructionCounter;

                Match matchinstr = scanInstruction.Match(instr);
                string opcode = matchinstr.Groups[1].Captures[0].Value;
                int A = int.Parse(matchinstr.Groups[2].Captures[0].Value);
                int B = int.Parse(matchinstr.Groups[3].Captures[0].Value);
                int C = int.Parse(matchinstr.Groups[4].Captures[0].Value);

                DoInstruction(registers, opcode, A, B, C);

                if (registerBoundToIP != -1)
                    instructionCounter = registers[registerBoundToIP];

                instructionCounter++;
                executed++;
            }

            PrintRegisters();
            Console.WriteLine("Execution halted on IP: " + instructionCounter);

            return "Answer: " + registers[0];
        }

        private static void PrintRegisters()
        {
            Console.Write("\t");
            for (int i = 0; i < registers.Count; i++)
                Console.Write(registers[i] + " ");
            Console.WriteLine();
        }

        private static void DoInstruction(List<int> registersState, string opcode, int A, int B, int C)
        {
            switch (opcode)
            {
                case "addr":
                    registersState[C] = registersState[A] + registersState[B];
                    break;
                case "addi":
                    registersState[C] = registersState[A] + B;
                    break;
                case "mulr":
                    registersState[C] = registersState[A] * registersState[B];
                    break;
                case "muli":
                    registersState[C] = registersState[A] * B;
                    break;
                case "banr":
                    registersState[C] = registersState[A] & registersState[B];
                    break;
                case "bani":
                    registersState[C] = registersState[A] & B;
                    break;
                case "borr":
                    registersState[C] = registersState[A] | registersState[B];
                    break;
                case "bori":
                    registersState[C] = registersState[A] | B;
                    break;
                case "setr":
                    registersState[C] = registersState[A];
                    break;
                case "seti":
                    registersState[C] = A;
                    break;
                case "gtir":
                    registersState[C] = (A > registersState[B]) ? 1 : 0;
                    break;
                case "gtri":
                    registersState[C] = (registersState[A] > B) ? 1 : 0;
                    break;
                case "gtrr":
                    registersState[C] = (registersState[A] > registersState[B]) ? 1 : 0;
                    break;
                case "eqir":
                    registersState[C] = (A == registersState[B]) ? 1 : 0;
                    break;
                case "eqri":
                    registersState[C] = (registersState[A] == B) ? 1 : 0;
                    break;
                case "eqrr":
                    registersState[C] = (registersState[A] == registersState[B]) ? 1 : 0;
                    break;
                default:
                    Console.WriteLine("Unknown opcode: " + opcode);
                    break;
            }
        }
    }
}
