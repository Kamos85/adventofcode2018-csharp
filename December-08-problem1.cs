﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December8Problem1
    {
        private class Node
        {
            public int childNodesCount;
            public int metaDataCount;
            public List<Node> childNodes = new List<Node>();
            public List<int> metaData = new List<int>();

            public Node()
            {
                childNodesCount = int.Parse(inputValues[readIndex]);
                metaDataCount = int.Parse(inputValues[readIndex+1]);
                readIndex += 2;

                for (int i = 0; i < childNodesCount; i++)
                    childNodes.Add(new Node());

                for (int i = 0; i < metaDataCount; i++)
                    metaData.Add(int.Parse(inputValues[readIndex+i]));

                readIndex += metaDataCount;
            }

            public int MetadataSum
            {
                get
                {
                    int sum = 0;
                    for (int i = 0; i < childNodesCount; i++)
                        sum += childNodes[i].MetadataSum;
                    for (int i = 0; i < metaDataCount; i++)
                        sum += metaData[i];
                    return sum;
                }
            }
        }

        //test input:
        //private static string input = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";
        static string input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem8-input.txt")[0];

        private static string[] inputValues = input.Split(' ');
        private static Node rootNode;
        private static int readIndex = 0;

        public static void Main1(string[] args)
        {
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static string SolveProblem()
        {
            rootNode = new Node();

            return rootNode.MetadataSum.ToString();
        }
    }
}
