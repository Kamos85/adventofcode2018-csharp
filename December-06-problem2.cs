﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December6Problem2
    {
        //test input:
//        private static string[] input =
//            {"1, 1",
//             "1, 6",
//             "8, 3",
//             "3, 4",
//             "5, 5",
//             "8, 9"};
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem6-input.txt");

        private class Point
        {
            public char id;
            public int x;
            public int y;

            public int fieldCount1 = 0;
            public int fieldCount2 = 0;
        }

        private static List<Point> points = new List<Point>();
        static List<List<char>> field = new List<List<char>>();

        private static int offsetX = 202;
        private static int offsetY = 202;

        public static void Main1(string[] args)
        {
            ParseInput();

            InitializeField(1005);
            int answer = ProcessField(10000);

            Console.WriteLine("ANSWER: " + answer);
        }

        private static void ParseInput()
        {
            Regex rx = new Regex(@"(\d+), (\d+)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Point point;
            for (int i = 0; i < input.Length; i++)
            {
                point = new Point();
                Match match = rx.Match(input[i]);

                point.x = int.Parse(match.Groups[1].Captures[0].Value);
                point.y = int.Parse(match.Groups[2].Captures[0].Value);
                point.id = (char)('A' + i);
                points.Add(point);
            }
        }

        private static void InitializeField(int size)
        {
            field.Clear();
            for (int i = 0; i < size; i++)
            {
                List<char> row = new List<char>();
                for (int j = 0; j < size; j++)
                    row.Add('.');
                field.Add(row);
            }
        }

        private static int ProcessField(int maxDistanceExclusive)
        {
            int area = 0;
            for (int y = 0; y < field.Count; ++y)
            {
                for (int x = 0; x < field.Count; ++x)
                {
                    int sum = 0;
                    for (int i = 0; i < points.Count; i++)
                    {
                        int dist = Math.Abs(x - (points[i].x+offsetX)) + Math.Abs(y - (points[i].y + offsetY));
                        sum += dist;
                    }

                    if (sum < maxDistanceExclusive)
                        area++;
                }
            }

            return area;
        }
    }
}
