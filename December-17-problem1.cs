﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December17Problem1
    {
        //test input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem17-test-input.txt");

        //real input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem17-input.txt");

        static char[][] field = new char[2000][];

        static int minx = 9999, maxx = -9999;
        static int miny = 9999, maxy = -9999;

        public static void Main1(string[] args)
        {
            for (int i = 0; i < field.Length; i++)
            {
                field[i] = new char[2000];
                for (int j = 0; j < field.Length; j++)
                    field[i][j] = '.';
            }

            ParseInput();
            Console.WriteLine(SolveProblem());
        }

        private static void ParseInput()
        {
            Regex inputRegex = new Regex(@"(.)=(\d+), (.)=(\d+)\.\.(\d+)",  //y=881, x=559..579
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            for (int i = 0; i < input.Length; i++)
            {
                Match instruction = inputRegex.Match(input[i]);
                string xory = instruction.Groups[1].Captures[0].Value;
                int xorycoord = int.Parse(instruction.Groups[2].Captures[0].Value);
                //string yorx = instruction.Groups[3].Captures[0].Value;
                int yorxcoord1 = int.Parse(instruction.Groups[4].Captures[0].Value);
                int yorxcoord2 = int.Parse(instruction.Groups[5].Captures[0].Value);

                for (int yx = yorxcoord1; yx <= yorxcoord2; yx++)
                {
                    if (xory == "x")
                        field[yx][xorycoord] = '#';
                    else
                        field[xorycoord][yx] = '#';
                }

                //Console.WriteLine(xory + ": " + xorycoord + ", " + yorx + ": " + yorxcoord1 + ".." + yorxcoord2);
            }
            for (int y = 0; y < field.Length; y++)
            {
                for (int x = 0; x < field.Length; x++)
                {
                    if (field[y][x] == '#')
                    {
                        if (x < minx)
                            minx = x;
                        if (x > maxx)
                            maxx = x;
                        if (y < miny)
                            miny = y;
                        if (y > maxy)
                            maxy = y;
                    }
                }
            }
        }

        private static string SolveProblem()
        {
            Stack<int> stackX = new Stack<int>();
            Stack<int> stackY = new Stack<int>();
            Stack<int> stackStatus = new Stack<int>();

            stackX.Push(500);
            stackY.Push(0);
            stackStatus.Push(0);

            int x, y, status;
            while (stackX.Count > 0)
            {
                x = stackX.Pop();
                y = stackY.Pop();
                status = stackStatus.Pop();

                if (x < minx-1 || x > maxx+1 || y > maxy)
                    continue;

                field[y][x] = '|';

                switch (status)
                {
                    case 0:
                        // drop
                        if (y < maxy)
                        {
                            stackX.Push(x);
                            stackY.Push(y);
                            stackStatus.Push(1);

                            if (field[y + 1][x] == '.')
                            {
                                stackX.Push(x);
                                stackY.Push(y+1);
                                stackStatus.Push(0);
                            }
                        }
                        break;

                    case 1:
                        // left
                        stackX.Push(x);
                        stackY.Push(y);
                        stackStatus.Push(2);

                        if (field[y][x-1] == '.' && field[y+1][x] != '|')
                        {
                            stackX.Push(x-1);
                            stackY.Push(y);
                            stackStatus.Push(0);
                        }
                        break;

                    case 2:
                        // right
                        if (field[y][x+1] == '.' && field[y+1][x] != '|')
                        {
                            stackX.Push(x+1);
                            stackY.Push(y);
                            stackStatus.Push(0);
                        }

                        if (field[y][x+1] == '#')
                            SolidifyWater(x, y);
                        break;
                }

                PrintField();
                Console.ReadLine();
            }

            //PrintField();
            return "Answer: " + CountWater();
        }

        private static int CountWater()
        {
            int waterCount = 0;
            for (int y = miny; y <= maxy; y++)
                for (int x = minx-1; x <= maxx+1; x++)
                    if (field[y][x] == '~' || field[y][x] == '|')
                        waterCount++;
            return waterCount;
        }

        private static void SolidifyWater(int x, int y)
        {
            int ix = x;
            bool solidify = true;
            do
            {
                if (field[y][ix] == '#')
                    break; // reached wall on the left side
                if (field[y + 1][ix] != '#' && field[y + 1][ix] != '~')
                    solidify = false; // solid ground
                ix--;
            } while (solidify);

            if (!solidify)
                return;

            for (int i = ix + 1; i <= x; i++)
                field[y][i] = '~';
        }

        private static void PrintField()
        {
            for (int y = miny; y <= maxy; y++)
            {
                for (int x = minx-1; x <= maxx+1; x++)
                {
                    if (field[y][x] == '|')
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                    else if (field[y][x] == '~')
                        Console.BackgroundColor = ConsoleColor.Blue;
                    else
                        Console.BackgroundColor = ConsoleColor.Black;
                    Console.Write(field[y][x]);
                }
                Console.WriteLine();
            }
            Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
