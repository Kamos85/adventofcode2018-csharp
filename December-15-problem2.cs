﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December15Problem2
    {
        //test input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem15-test-input.txt");

        //real input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem15-input.txt");

        private class Unit : IComparable
        {
            public enum UnitType
            {
                Elf,
                Goblin
            }

            public UnitType Type;
            public int X;
            public int Y;
            public int Health = 200;

            private int originalX;
            private int originalY;

            public Unit(UnitType type, int x, int y)
            {
                Type = type;
                X = x;
                Y = y;
                originalX = x;
                originalY = y;
            }

            private int attack
            {
                get
                {
                    if (Type == UnitType.Elf)
                        return elfAttackPower;
                    return 3;
                }
            }

            public bool Alive => Health > 0;

            int IComparable.CompareTo(object obj)
            {
                Unit other = (Unit)obj;

                if (Y < other.Y)
                    return -1;
                if (Y > other.Y)
                    return 1;

                if (X < other.X)
                    return -1;
                if (X > other.X)
                    return 1;

                return 0;
            }

            public void DoTurn()
            {
                Move();
                Attack();
            }

            public void Move()
            {
                bool canMove = FindClosestEnemy(this);
                if (canMove)
                {
                    X = stepToEnemyX;
                    Y = stepToEnemyY;
//                    Console.WriteLine(Type.ToString() + " moving to " + X + "," + Y);
                }
//                else
//                {
//                    Console.WriteLine(Type.ToString() + " can't move");
//                }
            }

            public void Attack()
            {
                Unit up = GetUnitAt(Y - 1, X);
                Unit left = GetUnitAt(Y, X - 1);
                Unit right = GetUnitAt(Y, X + 1);
                Unit down = GetUnitAt(Y + 1, X);

                List<Unit> adjacentUnits = new List<Unit>(){up, left, right, down};

                for (int i = adjacentUnits.Count - 1; i >= 0; i--)
                    if (adjacentUnits[i] == null)
                        adjacentUnits.RemoveAt(i);

                adjacentUnits.Sort();
                int minHealth = 999;
                Unit minHealthUnit = null;
                bool tie = false;
                for (int i = 0; i < adjacentUnits.Count; i++)
                {
                    if (adjacentUnits[i].Type != Type)
                    {
                        if (adjacentUnits[i].Health == minHealth)
                            tie = true;

                        if (adjacentUnits[i].Health < minHealth)
                        {
                            minHealth = adjacentUnits[i].Health;
                            minHealthUnit = adjacentUnits[i];
                            tie = false;
                        }
                    }
                }

                if (minHealthUnit == null)
                    return;

                if (tie)
                {
                    //Console.WriteLine("WE HAVE A TIE!");
                }

                minHealthUnit.Health -= attack;
            }

            public void Reset()
            {
                X = originalX;
                Y = originalY;
                Health = 200;
            }
        }

        private static bool[][] field;
        private static List<Unit> units = new List<Unit>();
        private static int round = 0;
        private static int elfAttackPower = 2;

        public static void Main1(string[] args)
        {
            ParseField();

            Console.WriteLine("CountAliveElfs: " + CountAliveElfs());
            bool DidAnyElfDie;
            do
            {
                for (int i = 0; i < units.Count; i++)
                    units[i].Reset();
                elfAttackPower++;

                DidAnyElfDie = DoBattle();
            } while (DidAnyElfDie);

            PrintField();
            Console.WriteLine("Final round: " + round);

            Unit.UnitType survivingType = Unit.UnitType.Elf;
            int healthSum = 0;
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].Alive)
                {
                    healthSum += units[i].Health;
                    survivingType = units[i].Type;
                }
            }
            Console.WriteLine("CountAliveElfs: " + CountAliveElfs());

            Console.WriteLine("Elf attack power: " + elfAttackPower);
            Console.WriteLine(survivingType + " win with health left: " + healthSum);
            Console.WriteLine("Answer: " + round * healthSum);
        }

        private static void ParseField()
        {
            field = new bool[input.Length][];
            for (int y = 0; y < input.Length; y++)
            {
                field[y] = new bool[input[y].Length];
                for (int x = 0; x < input[y].Length; x++)
                {
                    switch (input[y][x])
                    {
                        case '#':
                            field[y][x] = true;
                            break;
                        case '.':
                            field[y][x] = false;
                            break;
                        case 'E':
                            field[y][x] = false;
                            units.Add(new Unit(Unit.UnitType.Elf, x, y));
                            break;
                        case 'G':
                            field[y][x] = false;
                            units.Add(new Unit(Unit.UnitType.Goblin, x, y));
                            break;
                    }
                }
            }
        }

        private static int CountAliveElfs()
        {
            int alive = 0;
            for (int i = 0; i < units.Count; i++)
                if (units[i].Type == Unit.UnitType.Elf && units[i].Alive)
                    alive++;
            return alive;
        }

        private static void PrintHealths()
        {
            for (int i = 0; i < units.Count; i++)
                if (units[i].Alive)
                    Console.WriteLine(units[i].Type + ": " + units[i].Health);
        }

        private static bool DoBattle()
        {
            round = 0;
            int originalElfsAlive = CountAliveElfs();
            while (ElvesAndGoblinsAlive())
            {
                bool combatContinues = DoCombatRound();

                if (combatContinues)
                    round++;

//                PrintField();
//                Console.WriteLine("Round: " + round);
//                PrintHealths();
//                if (Console.ReadLine().Length > 0)
//                    break;
            }

            bool didElfsDie = originalElfsAlive > CountAliveElfs();
            return didElfsDie;
        }

        private static bool ElvesAndGoblinsAlive()
        {
            bool elf = false;
            bool goblin = false;
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].Alive)
                {
                    if (units[i].Type == Unit.UnitType.Elf)
                        elf = true;
                    else
                        goblin = true;
                }
            }

            return elf && goblin;
        }

        private static bool DoCombatRound()
        {
            units.Sort();
            for (int i = 0; i < units.Count; i++)
            {
                if (units[i].Alive)
                {
                    units[i].DoTurn();

                    if (!ElvesAndGoblinsAlive())
                    {
                        bool lastTurnInCombat = true;
                        for (int j = i + 1; j < units.Count; j++)
                            if (units[j].Alive)
                                lastTurnInCombat = false;

                        // if unit is the last to take a turn in combat then the round has been completed
                        return lastTurnInCombat;
                    }
                }
            }

            return true;
        }

        private static void PrintField()
        {
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    Unit unit = GetUnitAt(y, x);
                    if (unit != null)
                    {
                        if (unit.Type == Unit.UnitType.Elf)
                            Console.Write("E");
                        else
                            Console.Write("G");
                    }
                    else
                    {
                        Console.Write(field[y][x]?"#":".");
                    }
                }
                Console.WriteLine();
            }
        }

        private static Unit GetUnitAt(int y, int x)
        {
            for (int i = 0; i < units.Count; i++)
                if (units[i].X == x && units[i].Y == y)
                    if (units[i].Alive)
                        return units[i];
            return null;
        }

        private static int[][] distanceField;
        private static bool[][] leadsToEnemyField;

        // openX and openY should be in sync and describe a coordinate
        private static Queue<int> openX = new Queue<int>();
        private static Queue<int> openY = new Queue<int>();
        private static int stepToEnemyX = -1;
        private static int stepToEnemyY = -1;
        private static bool FindClosestEnemy(Unit unit)
        {
            if (distanceField == null)
                CreateDistanceField();
            ResetDistanceField();

            openX.Clear();
            openY.Clear();

            openX.Enqueue(unit.X);
            openY.Enqueue(unit.Y);

            int cx, cy;
            int currentLength = 0;
            List<Unit> enemyUnitsFound = new List<Unit>();
            while (openX.Count > 0 && enemyUnitsFound.Count == 0)
            {
                int dequeuesToDo = openX.Count;

                for (int i = 0; i < dequeuesToDo; i++)
                {
                    cx = openX.Dequeue();
                    cy = openY.Dequeue();

                    if (field[cy][cx]) // is wall
                        continue;

                    if (distanceField[cy][cx] > -999)
                        continue; // already visited node

                    Unit unitOnSpot = GetUnitAt(cy, cx);
                    if (unitOnSpot != null && unitOnSpot != unit)
                    {
                        if (unitOnSpot.Type == unit.Type)
                            continue;
                        enemyUnitsFound.Add(unitOnSpot);
                    }
                    else
                    { // empty spot or on starting unit's spot
                        openX.Enqueue(cx+1);
                        openY.Enqueue(cy);
                        openX.Enqueue(cx-1);
                        openY.Enqueue(cy);
                        openX.Enqueue(cx);
                        openY.Enqueue(cy+1);
                        openX.Enqueue(cx);
                        openY.Enqueue(cy-1);
                    }
                    distanceField[cy][cx] = currentLength;
                }

                if (enemyUnitsFound.Count == 0)
                    currentLength++;
            }

            if (enemyUnitsFound.Count == 0)
                return false;

            openX.Clear();
            openY.Clear();

            // get which coordinate should be taken by unit
            for (int i = 0; i < enemyUnitsFound.Count; i++)
            {
                openX.Enqueue(enemyUnitsFound[i].X);
                openY.Enqueue(enemyUnitsFound[i].Y);
            }

            //Console.WriteLine("currentlength: " + currentLength);
            //Console.WriteLine("openX: " + openX.Count);
            if (currentLength == 1)
                return false;

            while (currentLength > 0)
            {
                int dequeuesToDo = openX.Count;

                for (int i = 0; i < dequeuesToDo; i++)
                {
                    cx = openX.Dequeue();
                    cy = openY.Dequeue();

                    if (field[cy][cx]) // is wall
                        continue;

                    if (leadsToEnemyField[cy][cx])
                        continue; // already visited

                    if (distanceField[cy][cx] == -999)
                        continue; // never visited in initial phase

                    bool distanceFieldMatchesCurrentLength = distanceField[cy][cx] == currentLength;
                    if (distanceFieldMatchesCurrentLength)
                    {  // this is a coordinate on a path leading back to the unit
                        leadsToEnemyField[cy][cx] = true;

                        openX.Enqueue(cx+1);
                        openY.Enqueue(cy);
                        openX.Enqueue(cx-1);
                        openY.Enqueue(cy);
                        openX.Enqueue(cx);
                        openY.Enqueue(cy+1);
                        openX.Enqueue(cx);
                        openY.Enqueue(cy-1);
                    }
                }

                currentLength--;
            }

            cx = unit.X;
            cy = unit.Y;
            if (leadsToEnemyField[cy + 1][cx])
            {
                stepToEnemyX = cx;
                stepToEnemyY = cy + 1;
            }
            if (leadsToEnemyField[cy][cx + 1])
            {
                stepToEnemyX = cx + 1;
                stepToEnemyY = cy;
            }
            if (leadsToEnemyField[cy][cx - 1])
            {
                stepToEnemyX = cx - 1;
                stepToEnemyY = cy;
            }
            if (leadsToEnemyField[cy - 1][cx])
            {
                stepToEnemyX = cx;
                stepToEnemyY = cy - 1;
            }
            return true;
        }

        private static void CreateDistanceField()
        {
            distanceField = new int[field.Length][];
            leadsToEnemyField = new bool[field.Length][];
            for (int y = 0; y < input.Length; y++)
            {
                distanceField[y] = new int[field[0].Length];
                leadsToEnemyField[y] = new bool[field[0].Length];
            }
        }

        private static void ResetDistanceField()
        {
            for (int y = 0; y < distanceField.Length; y++)
            {
                for (int x = 0; x < distanceField[y].Length; x++)
                {
                    distanceField[y][x] = -999;
                    leadsToEnemyField[y][x] = false;
                }
            }
        }
    }
}
