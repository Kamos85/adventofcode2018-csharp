﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December9Problem2
    {
        //test input:
//        private static int players = 9;
//        private static int lastMarbleScore = 25;
        //test input:
//        private static int players = 13;
//        private static int lastMarbleScore = 7999;


        // real input
        private static int players = 423;
        private static int lastMarbleScore = 71944 * 100;

        static List<long> playerScores = new List<long>();
        static int currentPlayer = 0;

        static LinkedList<int> marbleCircle = new LinkedList<int>();
        static LinkedListNode<int> currentMarble;

        public static void Main1(string[] args)
        {
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static string SolveProblem()
        {
            for (int i = 0; i < players; i++)
                playerScores.Add(0);

            currentMarble = marbleCircle.AddFirst(0);

            for (int i = 1; i <= lastMarbleScore; i++)
            {
                if (i % 23 == 0)
                {
                    playerScores[currentPlayer] += i;

                    for (int j = 0; j < 7; j++)
                    {
                        if (currentMarble.Previous == null)
                            currentMarble = marbleCircle.Last;
                        else
                            currentMarble = currentMarble.Previous;
                    }

                    playerScores[currentPlayer] += currentMarble.Value;
                    LinkedListNode<int> nextMarble = currentMarble.Next;
                    if (nextMarble == null)
                        nextMarble = marbleCircle.First;
                    marbleCircle.Remove(currentMarble);
                    currentMarble = nextMarble;
                }
                else
                {
                    if (currentMarble.Next == null)
                        currentMarble = marbleCircle.First;
                    else
                        currentMarble = currentMarble.Next;

                    currentMarble = marbleCircle.AddAfter(currentMarble, i);
                }

                currentPlayer = (currentPlayer + 1) % players;

//                string rep = "";
//                foreach (var node in marbleCircle)
//                    rep += node + " ";
                //Console.WriteLine("[" + currentPlayer + "] " + rep);
            }
            return playerScores.Max().ToString();
        }
    }
}
