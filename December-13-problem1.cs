﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December13Problem1
    {
        //test input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem13-test-input.txt");

        //real input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem13-input.txt");

        private class Cart
        {
            public int X;
            public int Y;

            public int VX;
            public int VY;

            private int intersectionCounter = 0;

            public void Step()
            {
                Move();

                char token = input[Y][X];

                switch (token)
                {
                    case '-':
                    case '|':
                        // do nothing
                        break;

                    case '/':
                        if (VX != 0)
                            TurnLeft();
                        else  // VY != 0
                            TurnRight();
                        break;

                    case '\\':
                        if (VX != 0)
                            TurnRight();
                        else  // VY != 0
                            TurnLeft();
                        break;

                    case '+':
                        switch (intersectionCounter)
                        {
                            case 0:
                                TurnLeft();
                                break;
                            case 1:
                                // straight, do nothing
                                break;
                            case 2:
                                TurnRight();
                                break;
                        }

                        intersectionCounter = ++intersectionCounter % 3;
                        break;
                }
            }

            private void Move()
            {
                X += VX;
                Y += VY;
            }

            private void TurnLeft()
            {
                int oldVX = VX;
                VX = VY;
                VY = -oldVX;
            }

            private void TurnRight()
            {
                int oldVX = VX;
                VX = -VY;
                VY = oldVX;
            }
        }

        static List<Cart> carts = new List<Cart>();

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    char token = input[y][x];
                    switch (token)
                    {
                        case '>':
                            carts.Add(new Cart{X = x, Y = y, VX = 1, VY = 0});
                            input[y] = input[y].Remove(x, 1);
                            input[y] = input[y].Insert(x, "-");
                            break;
                        case '<':
                            carts.Add(new Cart{X = x, Y = y, VX = -1, VY = 0});
                            input[y] = input[y].Remove(x, 1);
                            input[y] = input[y].Insert(x, "-");
                            break;
                        case '^':
                            carts.Add(new Cart{X = x, Y = y, VX = 0, VY = -1});
                            input[y] = input[y].Remove(x, 1);
                            input[y] = input[y].Insert(x, "|");
                            break;
                        case 'v':
                            carts.Add(new Cart{X = x, Y = y, VX = 0, VY = 1});
                            input[y] = input[y].Remove(x, 1);
                            input[y] = input[y].Insert(x, "|");
                            break;
                    }
                }
            }
            Console.WriteLine("carts: " + carts.Count);
        }

        private static string SolveProblem()
        {
            int crashx = 0;
            int crashy = 0;
            int steps = 0;
            bool crash = false;
            while (true)
            {
                for (int i = 0; i < carts.Count; i++)
                {
                    carts[i].Step();

                    // check for crashes
                    for (int j = 0; j < carts.Count; j++)
                        for (int k = j+1; k < carts.Count; k++)
                            if (carts[j].X == carts[k].X && carts[j].Y == carts[k].Y)
                            {
                                crash = true;
                                crashx = carts[j].X;
                                crashy = carts[j].Y;
                            }

                    if (crash)
                        break;
                }

                if (crash)
                {
                    PrintField();
                    return "CRASH AT: " + crashx + "," + crashy;
                }

                //PrintField();
                //Console.ReadLine();

                steps++;
            }
        }

        private static void PrintField()
        {
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    bool hasCart = false;
                    for (int i = 0; i < carts.Count; i++)
                        if (carts[i].X == x && carts[i].Y == y)
                            hasCart = true;

                    if (hasCart)
                        Console.Write("X");
                    else
                        Console.Write(input[y][x]);
                }
                Console.WriteLine("");
            }
        }
    }
}
