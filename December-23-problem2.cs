﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December23Problem2
    {
        //test input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem23-input.txt");
//        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem23-test-input.txt");

        private class Drone
        {
            public int X;
            public int Y;
            public int Z;
            public int R;

            public bool InRangeOf(Drone drone)
            {
                int dx = Math.Abs(X - drone.X);
                int dy = Math.Abs(Y - drone.Y);
                int dz = Math.Abs(Z - drone.Z);
                return (dx + dy + dz) <= R;
            }

            public bool InRangeOf(double x, double y, double z, int accuracy)
            {
                double dx = Math.Abs(X - x);
                double dy = Math.Abs(Y - y);
                double dz = Math.Abs(Z - z);
                return (dx + dy + dz) <= (R + accuracy);
            }
        }

        private class DoublePosition
        {
            public double X;
            public double Y;
            public double Z;

            public DoublePosition(double x, double y, double z)
            {
                X = x;
                Y = y;
                Z = z;
            }
        }

        private class IntPosition
        {
            public int X;
            public int Y;
            public int Z;

            public IntPosition(int x, int y, int z)
            {
                X = x;
                Y = y;
                Z = z;
            }
        }

        private class Area
        {
            public int XMin;
            public int XMax;
            public int YMin;
            public int YMax;
            public int ZMin;
            public int ZMax;
            public int DroneRangesInArea;
            public int BucketSizeToUse;

            public override string ToString()
            {
                return "[" + XMin + ", " + XMax + "]"
                    + "[" + YMin + ", " + YMax + "]"
                    + "[" + ZMin + ", " + ZMax + "]";
            }
        }

        static List<Drone> drones = new List<Drone>();

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine(SolveProblem());
        }

        private static void Setup()
        {
            // pos=<15758255,29323050,44252572>, r=75313061
            Regex inputRegex = new Regex(@"pos=<(-?\d+),(-?\d+),(-?\d+)>, r=(\d+)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase);

            for (int i = 0; i < input.Length; i++)
            {
                Match instruction = inputRegex.Match(input[i]);
                int x = int.Parse(instruction.Groups[1].Captures[0].Value);
                int y = int.Parse(instruction.Groups[2].Captures[0].Value);
                int z = int.Parse(instruction.Groups[3].Captures[0].Value);
                int r = int.Parse(instruction.Groups[4].Captures[0].Value);

                drones.Add(new Drone{X = x, Y = y, Z = z, R = r});
            }
        }

        private static string SolveProblem()
        {
            int minX = drones.Min(drone => drone.X);
            int minY = drones.Min(drone => drone.Y);
            int minZ = drones.Min(drone => drone.Z);
            int maxX = drones.Max(drone => drone.X);
            int maxY = drones.Max(drone => drone.Y);
            int maxZ = drones.Max(drone => drone.Z);

            List<Area> toSearchAreas = new List<Area>();
            toSearchAreas.Add(new Area{XMin = minX, XMax = maxX, YMin = minY, YMax = maxY, ZMin = minZ, ZMax = maxZ,
                DroneRangesInArea = 0, BucketSizeToUse = 10000000});

            Area area;
            bool thereAreBigBuckets;
            do
            {
                int areaIndex = toSearchAreas.FindIndex(i => i.BucketSizeToUse >= 10);
                area = toSearchAreas[areaIndex];
                toSearchAreas.RemoveAt(areaIndex);

                Console.WriteLine("Searching area: " + area + "  -  using bucket size: " + area.BucketSizeToUse);

                List<Area> subAreas = AreaSearch(area);
                toSearchAreas.AddRange(subAreas);

                int maxDroneRangesInArea = toSearchAreas.Max(i => i.DroneRangesInArea);

                for (int i = toSearchAreas.Count - 1; i >= 0 ; i--)
                    if (toSearchAreas[i].DroneRangesInArea < maxDroneRangesInArea)
                        toSearchAreas.RemoveAt(i);

                thereAreBigBuckets = false;
                for (int i = 0; i < toSearchAreas.Count; i++)
                    if (toSearchAreas[i].BucketSizeToUse >= 10)
                        thereAreBigBuckets = true;

            } while (thereAreBigBuckets);

            Console.WriteLine("---- NOW FINE SEARCHING AREAS");

            IntPosition pos = new IntPosition(0,0,0);
            for (int i = 0; i < toSearchAreas.Count; i++)
            {
                Console.WriteLine("Fine searching area: " + area);
                pos = FineSearch(toSearchAreas[i]);
                Console.WriteLine("" + pos.X + ", " + pos.Y + ", " + pos.Z);
            }

            // attempted 124276100 = too low, was with floats, afterwards tried with double
            // attempted 124276103 = correct!!!

            return (Math.Abs(pos.X) + Math.Abs(pos.Y) + Math.Abs(pos.Z)).ToString();
        }

        private static List<Area> AreaSearch(Area area)
        {
            int bucketsX = (int)Math.Ceiling((double)(area.XMax - area.XMin) / (double)area.BucketSizeToUse);
            int bucketsY = (int)Math.Ceiling((double)(area.YMax - area.YMin) / (double)area.BucketSizeToUse);
            int bucketsZ = (int)Math.Ceiling((double)(area.ZMax - area.ZMin) / (double)area.BucketSizeToUse);

            int maxDroneRangesInArea = 0;
            List<DoublePosition> maxRangePositions = new List<DoublePosition>();

            for (int x = 0; x < bucketsX; x++)
            {
                for (int y = 0; y < bucketsY; y++)
                {
                    for (int z = 0; z < bucketsZ; z++)
                    {
                        double bucketXPos = area.XMin + x * area.BucketSizeToUse + area.BucketSizeToUse / 2f;
                        double bucketYPos = area.YMin + y * area.BucketSizeToUse + area.BucketSizeToUse / 2f;
                        double bucketZPos = area.ZMin + z * area.BucketSizeToUse + area.BucketSizeToUse / 2f;

                        int inRange = 0;
                        for (int i = 0; i < drones.Count; i++)
                            if (drones[i].InRangeOf(bucketXPos, bucketYPos, bucketZPos, (int)Math.Ceiling(area.BucketSizeToUse * 1.5f)))
                                inRange++;

                        if (inRange == maxDroneRangesInArea)
                        {
                            maxRangePositions.Add(new DoublePosition(bucketXPos, bucketYPos, bucketZPos));
                        }
                        else
                        {
                            if (inRange > maxDroneRangesInArea)
                            {
                                maxDroneRangesInArea = inRange;
                                maxRangePositions.Clear();
                                maxRangePositions.Add(new DoublePosition(bucketXPos, bucketYPos, bucketZPos));
                            }
                        }
                    }
                }
            }

            Console.WriteLine("    #positions with maxInRange: " + maxRangePositions.Count + " - maxInRange: " + maxDroneRangesInArea);

            double minManHatDist = double.MaxValue;
            List<Area> minimalDistAreas = new List<Area>();
            for (int i = 0; i < maxRangePositions.Count; i++)
            {
                double manHatDist = Math.Abs(maxRangePositions[i].X) + Math.Abs(maxRangePositions[i].Y) + Math.Abs(maxRangePositions[i].Z);

                if (Math.Abs(manHatDist - minManHatDist) < 0.001)
                {
                    minimalDistAreas.Add(new Area
                    {
                        XMin = (int) Math.Floor(maxRangePositions[i].X - area.BucketSizeToUse / 2f),
                        XMax = (int) Math.Ceiling(maxRangePositions[i].X + area.BucketSizeToUse / 2f),
                        YMin = (int) Math.Floor(maxRangePositions[i].Y - area.BucketSizeToUse / 2f),
                        YMax = (int) Math.Ceiling(maxRangePositions[i].Y + area.BucketSizeToUse / 2f),
                        ZMin = (int) Math.Floor(maxRangePositions[i].Z - area.BucketSizeToUse / 2f),
                        ZMax = (int) Math.Ceiling(maxRangePositions[i].Z + area.BucketSizeToUse / 2f),
                        DroneRangesInArea = maxDroneRangesInArea,
                        BucketSizeToUse = area.BucketSizeToUse / 10
                    });
                }
                else
                {
                    if (manHatDist < minManHatDist)
                    {
                        minManHatDist = manHatDist;
                        minimalDistAreas.Clear();
                        minimalDistAreas.Add(new Area
                        {
                            XMin = (int) Math.Floor(maxRangePositions[i].X - area.BucketSizeToUse / 2f),
                            XMax = (int) Math.Ceiling(maxRangePositions[i].X + area.BucketSizeToUse / 2f),
                            YMin = (int) Math.Floor(maxRangePositions[i].Y - area.BucketSizeToUse / 2f),
                            YMax = (int) Math.Ceiling(maxRangePositions[i].Y + area.BucketSizeToUse / 2f),
                            ZMin = (int) Math.Floor(maxRangePositions[i].Z - area.BucketSizeToUse / 2f),
                            ZMax = (int) Math.Ceiling(maxRangePositions[i].Z + area.BucketSizeToUse / 2f),
                            DroneRangesInArea = maxDroneRangesInArea,
                            BucketSizeToUse = area.BucketSizeToUse / 10
                        });
                    }
                }
            }

            Console.WriteLine("Areas returned: ");
            for (int i = 0; i < minimalDistAreas.Count; i++)
                Console.WriteLine(i + ": " + minimalDistAreas[i]);

            Console.WriteLine("");
            return minimalDistAreas;
        }

        private static IntPosition FineSearch(Area area)
        {
            int maxInRange = 0;
            List<IntPosition> maxRangePositions = new List<IntPosition>();

            for (int x = area.XMin; x <= area.XMax; x++)
            {
                for (int y = area.YMin; y <= area.YMax; y++)
                {
                    for (int z = area.ZMin; z <= area.ZMax; z++)
                    {
                        int inRange = 0;
                        for (int i = 0; i < drones.Count; i++)
                            if (drones[i].InRangeOf(x, y, z, 0))
                                inRange++;

                        if (inRange == maxInRange)
                        {
                            maxRangePositions.Add(new IntPosition(x, y, z));
                        }
                        else
                        {
                            if (inRange > maxInRange)
                            {
                                maxInRange = inRange;
                                maxRangePositions.Clear();
                                maxRangePositions.Add(new IntPosition(x, y, z));
                            }
                        }
                    }
                }
            }

            Console.WriteLine("maxCount: " + maxRangePositions.Count + " - maxInRange: " + maxInRange);

            double minManHatDist = double.MaxValue;
            List<IntPosition> minimalDistPositions = new List<IntPosition>();
            for (int i = 0; i < maxRangePositions.Count; i++)
            {
                double manHatDist = Math.Abs(maxRangePositions[i].X) + Math.Abs(maxRangePositions[i].Y) + Math.Abs(maxRangePositions[i].Z);

                if (Math.Abs(manHatDist - minManHatDist) < 0.001)
                {
                    minimalDistPositions.Add(maxRangePositions[i]);
                }
                else
                {
                    if (manHatDist < minManHatDist)
                    {
                        minManHatDist = manHatDist;
                        minimalDistPositions.Clear();
                        minimalDistPositions.Add(maxRangePositions[i]);
                    }
                }
            }

            if (minimalDistPositions.Count > 1)
            {
                Console.WriteLine("Multiple closest manhattan distances:");
                for (int i = 0; i < minimalDistPositions.Count; i++)
                    Console.WriteLine(i + ":" + minimalDistPositions[i].X + ", " + minimalDistPositions[i].Y + ", " + minimalDistPositions[i].Z);
            }

            return minimalDistPositions[0];
        }
    }
}
