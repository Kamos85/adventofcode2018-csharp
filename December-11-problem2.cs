﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December11Problem2
    {
        //test input:
        //private static int gridSerialNumber = 42;

        //real input
        private static int gridSerialNumber = 9435;


        private static int[][] powerGrid = new int[300][];
        private static int?[][] powerGridDialCache = new int?[300][];

        public static void Main1(string[] args)
        {
            Setup();
            Console.WriteLine("ANSWER: " + SolveProblem());
        }

        private static void Setup()
        {
            for (int i = 0; i < 300; ++i)
            {
                powerGrid[i] = new int[300];
                powerGridDialCache[i] = new int?[300];
            }

            for (int x = 0; x < 300; x++)
            {
                int xCoord = x + 1;
                for (int y = 0; y < 300; y++)
                {
                    int yCoord = y + 1;
                    int rackID = xCoord + 10;
                    int powerLevel = rackID * yCoord;
                    powerLevel += gridSerialNumber;
                    powerLevel *= rackID;
                    powerLevel = (powerLevel / 100) % 10;
                    powerLevel -= 5;
                    powerGrid[x][y] = powerLevel;
                }
            }
        }

        private static string SolveProblem()
        {
            int maxSum = -999;
            int sum = 0;
            int cx = -1, cy = -1, cdialSize = -1;
            for (int dialSize = 1; dialSize <= 300; dialSize++)
            {
                for (int x = 0; x < 300 - (dialSize - 1); x++)
                {
                    for (int y = 0; y < 300 - (dialSize - 1); y++)
                    {
                        if (powerGridDialCache[x][y] == null)
                            sum = 0;
                        else
                            sum = powerGridDialCache[x][y].Value;

                        for (int i = 0; i < dialSize; i++)
                            sum += powerGrid[x+i][y+(dialSize-1)];
                        for (int i = 0; i < dialSize-1; i++)  // -1, don't count corner double
                            sum += powerGrid[x+(dialSize-1)][y+i];

                        powerGridDialCache[x][y] = sum;

                        if (sum > maxSum)
                        {
                            maxSum = sum;
                            cx = x;
                            cy = y;
                            cdialSize = dialSize;
                        }
                    }
                }
            }

            return (cx+1) + "," + (cy+1) + "," + cdialSize + " -> " + maxSum;
        }
    }
}
