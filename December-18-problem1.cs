﻿using System;
using System.Collections.Generic;
using System.IO;

namespace AdventOfCode2018
{
    internal class December18Problem1
    {
        //test input
        //static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem18-test-input.txt");

        //real input
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem18-input.txt");

        static char[][] field;
        static char[][] tempField;

        public static void Main1(string[] args)
        {
            ParseInput();
            Console.WriteLine(SolveProblem());
        }

        private static void ParseInput()
        {
            field = new char[input.Length][];
            tempField = new char[input.Length][];

            for (int y = 0; y < input.Length; y++)
            {
                field[y] = new char[input[y].Length];
                tempField[y] = new char[input[y].Length];
                for (int x = 0; x < input[y].Length; x++)
                    field[y][x] = input[y][x];
            }
        }

        private static string SolveProblem()
        {
            for (int i = 0; i < 10; i++)
                DoRound();

            PrintField();
            return (CountField('|') * CountField('#')).ToString();
        }

        private static void DoRound()
        {
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                {
                    tempField[y][x] = field[y][x];
                    switch (field[y][x])
                    {
                        case '.':
                            if (CountThing(y, x, '|') >= 3)
                                tempField[y][x] = '|';
                            break;
                        case '|':
                            if (CountThing(y, x, '#') >= 3)
                                tempField[y][x] = '#';
                            break;
                        case '#':
                            if (CountThing(y, x, '#') >= 1
                                && CountThing(y, x, '|') >= 1)
                            {
                            }
                            else
                            {
                                tempField[y][x] = '.';
                            }
                            break;
                    }
                }
            }
            CopyField();
        }

        static List<int> neighbourX = new List<int>{-1,  0,  1, 1, 1, 0, -1, -1};
        static List<int> neighbourY = new List<int>{-1, -1, -1, 0, 1, 1,  1,  0};
        private static int CountThing(int y, int x, char c)
        {
            int count = 0;
            for (int i = 0; i < neighbourX.Count; i++)
            {
                try
                {
                    if (field[y + neighbourY[i]][x + neighbourX[i]] == c)
                        count++;
                }
                catch (Exception e)
                {
                }
            }

            return count;
        }

        private static void CopyField()
        {
            for (int y = 0; y < input.Length; y++)
                for (int x = 0; x < input[y].Length; x++)
                    field[y][x] = tempField[y][x];
        }

        private static int CountField(char countChar)
        {
            int count = 0;
            for (int y = 0; y < input.Length; y++)
                for (int x = 0; x < input[y].Length; x++)
                    if (field[y][x] == countChar)
                        count++;
            return count;
        }

        private static void PrintField()
        {
            for (int y = 0; y < input.Length; y++)
            {
                for (int x = 0; x < input[y].Length; x++)
                    Console.Write(field[y][x]);
                Console.WriteLine();
            }

            //Console.BackgroundColor = ConsoleColor.Black;
        }
    }
}
