﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode2018
{
    internal class December7Problem2
    {
        //test input:
//        private static string[] input =
//        {
//            "Step C must be finished before step A can begin.",
//            "Step C must be finished before step F can begin.",
//            "Step A must be finished before step B can begin.",
//            "Step A must be finished before step D can begin.",
//            "Step B must be finished before step E can begin.",
//            "Step D must be finished before step E can begin.",
//            "Step F must be finished before step E can begin."
//        };
        static string[] input = File.ReadAllLines("G:/Dev/csharp/AdventOfCode2018/Problem7-input.txt");

        static Dictionary<string, List<string>> dependencies = new Dictionary<string, List<string>>();

        static string steps = "";
        static int nextWorkerID = 0;

        static List<int> workerIDBusyForSeconds = new List<int>();
        static List<string> workerIDBusyOnStep = new List<string>();

        public static void Main1(string[] args)
        {
            //Console.WriteLine("TEST ANSWER: " + SolveProblem(2, 0));
            Console.WriteLine("ANSWER: " + SolveProblem(5, 60));
        }

        private static int WorkerSlotReady
        {
            get
            {
                for (int i = 0; i < workerIDBusyForSeconds.Count; i++)
                    if (workerIDBusyForSeconds[i] == 0)
                        return i;
                return -1;
            }
        }

        private static bool AnyWorkerWorking
        {
            get
            {
                for (int i = 0; i < workerIDBusyForSeconds.Count; i++)
                    if (workerIDBusyForSeconds[i] > 0)
                        return true;
                return false;
            }
        }

        private static string SolveProblem(int workers, int baseTime)
        {
            for (int i = 0; i < workers; i++)
            {
                workerIDBusyForSeconds.Add(0);
                workerIDBusyOnStep.Add("");
            }

            Regex rx = new Regex(@"Step (.) must be finished before step (.) can begin\.", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            for (int i = 0; i < input.Length; i++)
            {
                Match match = rx.Match(input[i]);
                string step1 = match.Groups[1].Captures[0].Value;
                string step2 = match.Groups[2].Captures[0].Value;

                if (!dependencies.ContainsKey(step2))
                    dependencies[step2] = new List<string>();

                if (!dependencies.ContainsKey(step1))
                    dependencies[step1] = new List<string>();

                dependencies[step2].Add(step1);
            }

            int seconds = 0;
            do
            {
                // see if work can be done
                do
                {
                    int workerSlotReady = WorkerSlotReady;
                    if (workerSlotReady == -1)
                        break;

                    if (!DoStepIfAvailable(workerSlotReady, baseTime))
                        break;
                } while (true);

                // step time
                for (int i = 0; i < workerIDBusyForSeconds.Count; i++)
                {
                    if (workerIDBusyForSeconds[i] > 0)
                        workerIDBusyForSeconds[i] -= 1;
                    if (workerIDBusyForSeconds[i] <= 0)
                    {
                        foreach (var pair in dependencies)
                            pair.Value.Remove(workerIDBusyOnStep[i]);

                        workerIDBusyOnStep[i] = "";
                    }
                }
                ++seconds;
            } while (dependencies.Count > 0 || AnyWorkerWorking);

            return seconds.ToString();
        }

        private static bool DoStepIfAvailable(int workerSlot, int baseTime)
        {
            var elligibleSteps = dependencies.Where(pair => pair.Value.Count == 0);
            var firstStep = "ZZZ";
            foreach (var pair in elligibleSteps)
                if (firstStep.CompareTo(pair.Key) > 0)
                    firstStep = pair.Key;

            if (firstStep == "ZZZ")
                return false;

            dependencies.Remove(firstStep);
            steps += firstStep;

            // Set worker to work
            workerIDBusyForSeconds[workerSlot] = (int)firstStep[0] - 'A' + 1 + baseTime;
            workerIDBusyOnStep[workerSlot] = firstStep;

            return true;
        }
    }
}
